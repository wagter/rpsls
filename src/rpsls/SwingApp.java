package rpsls;

import rpsls.core.container.Container;
import rpsls.core.container.ContainerFactory;
import rpsls.core.container.UnknownParameterKeyException;
import rpsls.core.container.UnresolvableServiceException;
import rpsls.core.launcher.GameLauncher;

import rpsls.swing.container.SwingContainerConfigurator;

public class SwingApp
{
    public static void main( String... args )
    {
        // enable text antialiasing
        System.setProperty( "awt.useSystemAAFontSettings", "on" );
        System.setProperty( "rpsls.swing.aatext", "true" );

        Container container = ContainerFactory.createCoreContainer();
        container.configure( new SwingContainerConfigurator() );

        try {
            container.get( GameLauncher.class ).launch();
        } catch ( UnresolvableServiceException | UnknownParameterKeyException e ) {
            e.printStackTrace();
        }
    }
}
