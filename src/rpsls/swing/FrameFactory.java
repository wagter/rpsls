package rpsls.swing;

import javax.swing.*;

/**
 * Factory class to create JFrame instances
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class FrameFactory
{
    /**
     * Create a regular JFrame
     *
     * @return a regular JFrame instance
     */
    public static JFrame createRegularFrame()
    {
        JFrame frame = new JFrame( "Rock, Paper, Scissors, Lizard, Spock" );
        frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        frame.setSize( new java.awt.Dimension( 900, 900 ) );

        return frame;
    }
}
