package rpsls.swing.view;

import rpsls.core.view.View;

import javax.swing.*;

/**
 * Base panel for extending panel views
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public abstract class BasePanelView extends JPanel implements View
{
    /**
     * BasePanelView constructor
     */
    BasePanelView()
    {
        initLayout();
    }

    @Override
    public void render()
    {
        repaint();
    }

    /**
     * Initialize layout
     */
    protected void initLayout()
    {
    }
}
