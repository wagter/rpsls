package rpsls.swing.view;

import rpsls.core.score.ScoreBoard;
import rpsls.core.util.Position;

import rpsls.swing.view.drawable.ScoreDrawableFactory;
import rpsls.swing.view.drawable.Drawable;
import rpsls.swing.view.drawable.DrawableArray;

import javax.swing.*;
import java.awt.*;

/**
 * View for displaying a ScoreBoard instance
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class ScoreBoardView extends BasePanelView
{
    private DrawableArray avatarScoreDrawables;
    private Font labelFont;

    /**
     * ScoreBoardView constructor
     *
     * @param scoreBoard the ScoreBoard instance to display
     */
    public ScoreBoardView( ScoreBoard scoreBoard, Font font )
    {
        super();
        labelFont = font.deriveFont( (float) 22.0 );
        avatarScoreDrawables = ScoreDrawableFactory.create( scoreBoard, font );
        add( getLabelPanel() );
    }

    @Override
    public void paintComponent( Graphics g )
    {
        super.paintComponent( g );
        Graphics2D g2d = (Graphics2D) g;
        int index = 0;
        for ( Drawable avatarScoreDrawable : avatarScoreDrawables ) {
            avatarScoreDrawable.draw( g2d, new Position( 0, 70 + 72 * index ) );
            index++;
        }
    }

    @Override
    protected void initLayout()
    {
        setPreferredSize( new Dimension( 270, 500 ) );
        setBackground( Color.decode( "#212121" ) );
    }

    /**
     * Create and return the label panel
     *
     * @return the label panel
     */
    private JPanel getLabelPanel()
    {
        return new JPanel()
        {{
            setPreferredSize( new Dimension( 200, 30 ) );
            setBackground( Color.decode( "#212121" ) );
            add( new JLabel( "S" )
            {{
                setForeground( Color.decode( "#ffd5d5" ) );
                setFont( labelFont );
            }} );
            add( new JLabel( "C" )
            {{
                setForeground( Color.decode( "#fff6d5" ) );
                setFont( labelFont );
            }} );
            add( new JLabel( "O" )
            {{
                setForeground( Color.decode( "#f6d5ff" ) );
                setFont( labelFont );
            }} );
            add( new JLabel( "R" )
            {{
                setForeground( Color.decode( "#d7f4d7" ) );
                setFont( labelFont );
            }} );
            add( new JLabel( "E" )
            {{
                setForeground( Color.decode( "#d5e5ff" ) );
                setFont( labelFont );
            }} );
        }};
    }
}
