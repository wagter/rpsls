package rpsls.swing.view;

import javax.swing.*;
import java.awt.*;

/**
 * View for displaying the sidebar
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class SideBarView extends BasePanelView
{
    /**
     * SidebarView constructor
     *
     * @param scoreBoardView the ScoreBoardView instance
     */
    public SideBarView( ScoreBoardView scoreBoardView )
    {
        super();
        add( scoreBoardView, new GridBagConstraints() );
    }

    @Override
    protected void initLayout()
    {
        setLayout( new GridBagLayout() );
        setBackground( Color.decode( "#212121" ) );
        setBorder( BorderFactory.createEmptyBorder( 16, 16, 16, 16 ) );
    }
}
