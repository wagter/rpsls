package rpsls.swing.view;

import rpsls.core.util.Dimension;
import rpsls.core.util.Position;

import rpsls.swing.view.drawable.Drawable;

import java.awt.*;

/**
 * View for displaying the viewport
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class ViewportView extends BasePanelView
{
    private Drawable drawable;

    /**
     * ViewportView constructor
     *
     * @param drawable the Drawable element
     */
    public ViewportView( Drawable drawable )
    {
        super();
        this.drawable = drawable;
    }

    @Override
    protected void initLayout()
    {
        setBackground( Color.decode( "#212121" ) );
    }

    @Override
    public void paintComponent( Graphics g )
    {
        super.paintComponent( g );
        drawable.draw(
            (Graphics2D) g,
            new Position( 0, 0 ),
            new Dimension( getWidth(), getHeight() )
        );
    }
}