package rpsls.swing.view.drawable;

import rpsls.core.avatar.Avatar;

import java.util.Collection;

/**
 * Factory class to create AvatarDrawable instances
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class AvatarDrawableFactory
{
    public static DrawableArray create( Collection<Avatar> avatars )
    {
        DrawableArray drawables = new DrawableArray();

        for ( Avatar avatar : avatars ) {
            drawables.add( create( avatar ) );
        }

        return drawables;
    }

    public static AvatarDrawable create( Avatar avatar )
    {
        return new AvatarDrawable( avatar );
    }
}
