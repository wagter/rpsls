package rpsls.swing.view.drawable;

import rpsls.core.state.GameState;
import rpsls.core.util.Dimension;
import rpsls.core.util.Position;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Pause screen overlay
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class PauseDrawable extends AbstractDrawable
{
    private GameState gameState;
    private BufferedImage image;
    private Color bgColor;
    private float alpha = 0;

    /**
     * PauseDrawable constructor
     *
     * @param gameState the GameState instance
     */
    public PauseDrawable( GameState gameState )
    {
        this.gameState = gameState;
        this.image = getImage();
        this.bgColor = new Color( 33, 33, 33, 172 );
    }

    @Override
    public void draw( Graphics2D g2d, Position origin, Dimension parentDimension )
    {
        if ( gameState.isPaused() || (!gameState.isPaused() && alpha > 0 ) ) {

            g2d.setColor( bgColor );
            g2d.setComposite( AlphaComposite.getInstance( AlphaComposite.SRC_OVER, 1 ) );

            g2d.fillRect(
                (int) origin.getX(),
                (int) origin.getY(),
                parentDimension.getWidth(),
                parentDimension.getHeight()
            );

            int x = (int) origin.getX();
            int y = parentDimension.getHeight() - image.getHeight();

            if ( gameState.isPaused() && alpha < 1 ) {
                alpha += 0.1;
            } else if ( !gameState.isPaused() && alpha > 0 ) {
                alpha -= 0.1;
            }

            if ( alpha > 1 ) {
                alpha = 1;
            } else if ( alpha < 0 ) {
                alpha = 0;
            }

            g2d.setComposite( AlphaComposite.getInstance( AlphaComposite.SRC_OVER, alpha ) );
            g2d.drawImage( image, x, y, null );
            g2d.setComposite( AlphaComposite.getInstance( AlphaComposite.SRC_OVER, 1 ) );
        }
    }

    /**
     * Get the image for the overlay
     *
     * @return the image for the overlay
     */
    private BufferedImage getImage()
    {
        try {
            return ImageIO.read( getClass().getResource( "/res/img/sheldon.png" ) );
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        return null;
    }
}