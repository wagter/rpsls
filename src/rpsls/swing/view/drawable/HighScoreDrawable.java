package rpsls.swing.view.drawable;

import rpsls.core.score.ScoreBoard;
import rpsls.core.util.Dimension;
import rpsls.core.util.Position;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Drawable for displaying the total high score
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class HighScoreDrawable extends AbstractDrawable
{
    private ScoreBoard scoreBoard;
    private BufferedImage image;
    private Font font;
    private Color bgColor, textColor;

    /**
     * HighScoreDrawable constructor
     *
     * @param scoreBoard the ScoreBoard instance
     */
    public HighScoreDrawable( ScoreBoard scoreBoard, Font font )
    {
        this.scoreBoard = scoreBoard;
        this.image = getImage();
        this.bgColor = Color.decode( "#303030" );
        this.textColor = Color.decode( "#e8b328" );
        this.font = font.deriveFont( (float) 25 );
    }

    @Override
    public void draw( Graphics2D g2d, Position origin, Dimension parentDimension )
    {
        Long highScore = scoreBoard.getHighScore();

        g2d.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
        g2d.setColor( bgColor );
        g2d.fillOval( (int) origin.getX(), (int) origin.getY(), 64, 64 );
        g2d.fillOval( (int) origin.getX() + 200, (int) origin.getY(), 64, 64 );
        g2d.fillRect( (int) origin.getX() + 32, (int) origin.getY(), 200, 64 );

        g2d.setColor( textColor );
        g2d.setRenderingHint( RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON );

        if ( font != null ) {
            g2d.setFont( font );
        }

        g2d.drawString(
            highScore != null ? highScore.toString() + " ms" : "-",
            (int) origin.getX() + 72,
            (int) origin.getY() + 40
        );

        if ( image == null ) {
            return;
        }

        g2d.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
        g2d.drawImage( image, (int) origin.getX() + 8, (int) origin.getY() + 8, null );

    }

    /**
     * Get the image for the high score drawable
     *
     * @return the image for the high score drawable
     */
    private BufferedImage getImage()
    {
        try {
            return ImageIO.read( getClass().getResource( "/res/img/star.png" ) );
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        return null;
    }
}
