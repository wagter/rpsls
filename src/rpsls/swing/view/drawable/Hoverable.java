package rpsls.swing.view.drawable;

/**
 * Defines a hoverable element
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface Hoverable
{
    /**
     * Get the current hover state
     *
     * @return the current hover state
     */
    boolean getHover();

    /**
     * Set the new hover state
     *
     * @param hover the new hover state
     */
    void setHover( boolean hover );
}
