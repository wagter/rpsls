package rpsls.swing.view.drawable;

import rpsls.core.score.Score;
import rpsls.core.score.ScoreBoard;

import java.awt.*;
import java.util.Iterator;

/**
 * Factory class to create ScoreDrawable instances
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class ScoreDrawableFactory
{
    /**
     * Create a ScoreDrawable instance for a Score instance
     *
     * @param score the Score instance
     * @return the ScoreDrawable instance
     */
    public static ScoreDrawable create( Score score, Font font )
    {
        return new ScoreDrawable( score, font );
    }

    /**
     * Create a DrawableArray instance for a ScoreBoard instance
     *
     * @param scoreBoard the ScoreBoard instance
     * @return the DrawableArray instance
     */
    public static DrawableArray create( ScoreBoard scoreBoard, Font font )
    {
        return new DrawableArray()
        {{
            Iterator<Score> iterator = scoreBoard.getIterator();
            add( new HighScoreDrawable( scoreBoard, font ));
            while ( iterator.hasNext() ) {
                add( create( iterator.next(), font ));
            }
        }};
    }
}
