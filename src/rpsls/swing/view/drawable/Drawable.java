package rpsls.swing.view.drawable;

import rpsls.core.util.Dimension;
import rpsls.core.util.Position;

import java.awt.*;

/**
 * Interfaces that defines that an object is drawable
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface Drawable
{
    /**
     * Draw the object
     *
     * @param g2d the Graphics2D instance of the component to draw in
     */
    void draw( Graphics2D g2d );

    /**
     * Draw the object from a specific origin
     *
     * @param g2d the Graphics2D instance of the component to draw in
     * @param origin the x and y origin from which to start drawing
     */
    void draw( Graphics2D g2d, Position origin );

    /**
     * Draw the object from a specific origin within its parents dimension
     *
     * @param g2d the Graphics2D instance of the component to draw in
     * @param origin the x and y origin from which to start drawing
     * @param parentDimension the parent's dimension
     */
    void draw ( Graphics2D g2d, Position origin, Dimension parentDimension );
}
