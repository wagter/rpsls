package rpsls.swing.view.drawable;

import rpsls.core.avatar.Avatar;
import rpsls.core.util.Dimension;
import rpsls.core.util.Position;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Drawable element representing an Avatar instance
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class AvatarDrawable extends AbstractDrawable
{
    private Avatar avatar;
    private BufferedImage image;

    /**
     * AvatarDrawable constructor
     *
     * @param avatar the Avatar instance to represent by the Drawable
     */
    public AvatarDrawable( Avatar avatar )
    {
        this.avatar = avatar;
        this.image = getImage();
    }

    @Override
    public void draw( Graphics2D g2d, Position origin, Dimension parentDimension )
    {
        if ( image == null ) {
            return;
        }
        AlphaComposite ac = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, avatar.getAlpha() );
        g2d.setComposite(ac);
        g2d.drawImage(
            image,
            (int) Math.round( origin.getX() + avatar.getPosition().getX() ),
            (int) Math.round( origin.getY() + avatar.getPosition().getY() ),
            null
        );
    }

    /**
     * Get the image for the Avatar instance
     *
     * @return the image for the Avatar instance
     */
    private BufferedImage getImage()
    {
        try {
            return ImageIO.read( getClass().getResource( String.format( "/res/img/%s.png", avatar.getName() ) )  );
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        return null;
    }
}
