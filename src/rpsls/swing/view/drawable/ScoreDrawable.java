package rpsls.swing.view.drawable;

import rpsls.core.score.Score;
import rpsls.core.util.Dimension;
import rpsls.core.util.Position;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Drawable to display an Avatar's score
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class ScoreDrawable extends AbstractDrawable
{
    private Score score;
    private BufferedImage image;
    private Font smallFont, largeFont;
    private Color bgColor, wrongColor, textColor;

    /**
     * ScoreDrawable constructor
     *
     * @param score the Score instance
     */
    public ScoreDrawable( Score score, Font font )
    {
        this.score = score;
        this.image = getImage();
        this.bgColor = Color.decode( "#303030" );
        this.wrongColor = Color.decode( "#d50000" );
        this.textColor = Color.decode( score.getAvatar().getColor() );
        this.smallFont = font.deriveFont( (float) 14.0 );
        this.largeFont = font.deriveFont( (float) 18.0 );
    }

    @Override
    public void draw( Graphics2D g2d, Position origin, Dimension parentDimension )
    {
        drawBg( g2d, origin );
        drawImg( g2d, origin );
        drawScore( g2d, origin );
        drawWrong( g2d, origin );
    }

    /**
     * Draw the background
     *
     * @param g2d the parent component's Graphics2D object
     * @param origin the drawable's origin
     */
    private void drawBg( Graphics2D g2d, Position origin )
    {
        g2d.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
        g2d.setColor( bgColor );

        g2d.fillOval( (int) origin.getX(), (int) origin.getY(), 64, 64 );
        g2d.fillOval( (int) origin.getX() + 200, (int) origin.getY(), 64, 64 );
        g2d.fillRect( (int) origin.getX() + 32, (int) origin.getY(), 200, 64 );
    }

    /**
     * Draw the Avatar's image
     *
     * @param g2d the parent component's Graphics2D object
     * @param origin the drawable's origin
     */
    private void drawImg( Graphics2D g2d, Position origin )
    {
        if ( image == null ) {
            return;
        }

        g2d.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
        g2d.drawImage( image, (int) origin.getX() + 8, (int) origin.getY() + 8, null );
    }

    /**
     * Draw the score numbers
     *
     * @param g2d the parent component's Graphics2D object
     * @param origin the drawable's origin
     */
    private void drawScore( Graphics2D g2d, Position origin )
    {
        Long highScore = score.getHighScore();
        Long lastScore = score.getLastScore();

        g2d.setColor( textColor );

        g2d.setRenderingHint( RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON );


        g2d.setFont( largeFont );

        g2d.drawString(
            highScore != null ? highScore.toString() + " ms" : "-",
            (int) origin.getX() + 72,
            (int) origin.getY() + 26
        );

        g2d.setFont( smallFont );

        g2d.drawString(
            lastScore != null ? lastScore.toString() + " ms" : "-",
            (int) origin.getX() + 72,
            (int) origin.getY() + 48
        );
    }

    /**
     * Draw the wrong answers badge
     *
     * @param g2d the parent component's Graphics2D object
     * @param origin the drawable's origin
     */
    private void drawWrong( Graphics2D g2d, Position origin )
    {
        if ( score.getWrong() < 1 ) {
            return;
        }

        String wrongString = "" + score.getWrong();

        // determine the x and y for the string to position in center
        // see: https://stackoverflow.com/a/27740330/8108407
        FontMetrics metrics = g2d.getFontMetrics( largeFont );

        Rectangle rect = new Rectangle( (int) origin.getX() + 200 + 8, (int) origin.getY() + 8, 48, 48 );
        int x = rect.x + ( rect.width - metrics.stringWidth( wrongString ) ) / 2;
        int y = rect.y + ( ( rect.height - metrics.getHeight() ) / 2 ) + metrics.getAscent();

        g2d.setColor( wrongColor );
        g2d.fillOval( rect.x, rect.y, rect.width, rect.height );

        g2d.setColor( Color.WHITE );
        g2d.setFont( largeFont );
        g2d.drawString( wrongString, x, y );

    }

    /**
     * Get the Avatar's image
     *
     * @return the Avatar's image
     */
    private BufferedImage getImage()
    {
        try {
            return ImageIO.read( getClass().getResource( String.format( "/res/img/%s-md.png", score.getAvatar().getName() ) ) );
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        return null;
    }
}
