package rpsls.swing.view.drawable;

import rpsls.core.util.Dimension;
import rpsls.core.util.Position;

import java.awt.*;
import java.util.ArrayList;

/**
 * An array containing drawable objects
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class DrawableArray extends ArrayList<Drawable> implements Drawable
{
    @Override
    public void draw( Graphics2D g2d )
    {
        draw( g2d, new Position( 0, 0 ) );
    }

    @Override
    public void draw( Graphics2D g2d, Position origin )
    {
        draw( g2d, origin, new Dimension( 0, 0 ) );
    }

    @Override
    public void draw( Graphics2D g2d, Position origin, Dimension parentDimension )
    {
        for ( Drawable drawable : this ) {
            drawable.draw( g2d, origin, parentDimension );
        }
    }
}
