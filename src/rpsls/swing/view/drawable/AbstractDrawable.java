package rpsls.swing.view.drawable;

import rpsls.core.util.Dimension;
import rpsls.core.util.Position;

import java.awt.*;

/**
 * Abstract class to extend drawable elements from
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public abstract class AbstractDrawable implements Drawable
{
    @Override
    public void draw( Graphics2D g2d )
    {
        draw( g2d, new Position( 0, 0 ) );
    }

    @Override
    public void draw( Graphics2D g2d, Position origin )
    {
        draw( g2d, origin, new Dimension( 0, 0 ) );
    }
}
