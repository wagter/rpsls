package rpsls.swing.view;

import rpsls.core.hint.Hints;
import rpsls.core.hint.Hint;
import rpsls.core.view.View;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * View for displaying hint messages
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class HintsView extends JPanel implements View
{
    private Map<Hint, JLabel> hintLabels;
    private Font font;
    private JLabel defaultHint;
    private Map<Hint, Color> hintColors = new HashMap<>(  );

    public HintsView( Hints hints, Font font )
    {
        this.font = font.deriveFont( (float) 14.0 );
        initLayout();
        initLabels( hints );

        for (Hint hint : hints.getHints()) {
            hintColors.put( hint, Color.decode( hint.getBeater().getColor() ) );
        }
    }

    @Override
    public void render()
    {
        repaint();
    }

    @Override
    public void paintComponent( Graphics g )
    {
        super.paintComponent( g );

        Iterator iterator = hintLabels.entrySet().iterator();

        int visibleHints = 0;
        while ( iterator.hasNext() ) {

            Map.Entry pair = (Map.Entry) iterator.next();
            Hint hint = (Hint) pair.getKey();
            JLabel label = (JLabel) pair.getValue();
            remove( label );

            if ( hint.getBeaten().isSpawned() ) {
                label.setForeground( hintColors.get( hint ) );
                if ( font != null ) {
                    label.setFont( font );
                }
                add( label );
                visibleHints++;
            }
        }

        if ( visibleHints < 1 ) {
            if ( font != null ) {
                defaultHint.setFont( font );
            }
            defaultHint.setForeground( Color.WHITE );
            add( defaultHint );
        } else {
            remove( defaultHint );
        }
    }

    /**
     * Initialize labels
     *
     * @param hints the Hints instance
     */
    private void initLabels( Hints hints )
    {

        defaultHint = new JLabel( "Wait for a figure to appear, click the correct button to beat it as fast as you can!" );
        hintLabels = new HashMap<Hint, JLabel>()
        {{
            for ( Hint hint : hints.getHints() ) {
                JLabel label = new JLabel( " " + hint.getHint() + " " );
                label.setBorder( BorderFactory.createEmptyBorder( 8, 8, 8, 8 ) );


                put( hint, label );
            }
        }};
    }

    /**
     * Initialize the layout
     */
    private void initLayout()
    {
        setPreferredSize( new Dimension( 600, 32 ) );
        setBackground( Color.decode( "#212121" ) );
        setBackground( Color.decode( "#303030" ) );
    }

}
