package rpsls.swing.view;

import rpsls.swing.view.button.ControlButton;

import java.awt.*;
import java.util.List;

public class ControlButtonView extends BasePanelView
{
    public ControlButtonView( List<ControlButton> controlButtons )
    {
        super();
        for ( ControlButton controlButton : controlButtons ) {
            add( controlButton );
        }
    }

    @Override
    protected void initLayout()
    {
        setBackground( Color.decode( "#303030" ) );
    }
}
