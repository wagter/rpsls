package rpsls.swing.view.button;

import rpsls.core.avatar.Avatar;
import rpsls.core.util.Position;
import rpsls.core.util.Dimension;

import rpsls.swing.view.drawable.Drawable;
import rpsls.swing.view.drawable.Hoverable;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * A button representing an Avatar instance
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class AvatarButton extends JComponent implements Hoverable, Drawable
{
    private Avatar avatar;
    private BufferedImage image;
    private boolean hover = false;
    private final java.awt.Dimension size = new java.awt.Dimension( 80, 100 );
    private final Color bgColor, hoverColor;
    private final Font font;

    /**
     * AvatarButton constructor
     *
     * @param avatar the Avatar instance to be represented by the button
     */
    public AvatarButton( Avatar avatar, Font font )
    {
        this.avatar = avatar;
        this.image = getImage();
        this.bgColor = Color.decode( "#303030" );
        this.hoverColor = Color.decode( "#212121" );
        this.font = font;

        initLayout();
    }

    /**
     * Get the Avatar instance represented by the button
     *
     * @return the Avatar instance represented by the button
     */
    public Avatar getAvatar()
    {
        return avatar;
    }

    @Override
    public void paintComponent( Graphics g )
    {
        super.paintComponent( g );
        draw( (Graphics2D) g );
    }

    @Override
    public boolean getHover()
    {
        return !avatar.isSpawned() && hover;
    }

    @Override
    public void setHover( boolean hover )
    {
        this.hover = hover;
    }

    @Override
    public void draw( Graphics2D g2d )
    {
        draw( g2d, new Position( 0,0 ));
    }

    @Override
    public void draw( Graphics2D g2d, Position origin )
    {
        draw( g2d, origin, new Dimension( 0,0 ) );
    }

    @Override
    public void draw( Graphics2D g2d, Position origin, Dimension parentDimension )
    {
        float alpha = (float) ( avatar.isSpawned() ? 0.3 : 1.0 );

        AlphaComposite ac = AlphaComposite.getInstance( AlphaComposite.SRC_OVER, alpha );
        g2d.setComposite( ac );

        g2d.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
        g2d.setColor( getHover() ? hoverColor : bgColor );
        g2d.fillRoundRect( 0, 0, (int) size.getWidth(), (int) size.getHeight(), 16, 16 );

        if ( image == null ) {
            return;
        }

        g2d.drawImage( image, 8, 8, null );
    }

    /**
     * Initialize the layout
     */
    private void initLayout()
    {
        setPreferredSize( size );
        setLayout( new BorderLayout() );
        setBorder( BorderFactory.createEmptyBorder( 0, 0, 6, 0 ) );
        add( createLabel(), BorderLayout.SOUTH );
    }

    /**
     * Get the image for the Avatar instance
     *
     * @return the image for the Avatar instance
     */
    private BufferedImage getImage()
    {
        try {
            return ImageIO.read( getClass().getResource( String.format( "/res/img/%s.png", avatar.getName() ) ) );
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Create the label for the button
     *
     * @return the label for the button
     */
    private JLabel createLabel()
    {
        JLabel label = new JLabel( avatar.getName() );
        label.setForeground( Color.decode( avatar.getColor() ) );
        label.setHorizontalAlignment( JLabel.CENTER );
        label.setFont( font.deriveFont( (float)16.0 ) );

        return label;
    }
}
