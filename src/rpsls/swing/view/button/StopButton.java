package rpsls.swing.view.button;

import rpsls.core.util.Dimension;
import rpsls.core.util.Position;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * A button to trigger a stop rpsls.core action
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class StopButton extends ControlButton
{
    private BufferedImage stopIcon, stopIconHover;

    /**
     * StopButton constructor
     */
    public StopButton()
    {
        super();

        stopIcon = getStopIcon();
        stopIconHover = getStopIconHover();
    }

    @Override
    public void draw( Graphics2D g2d, Position origin, Dimension parentDimension )
    {
        g2d.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
        g2d.drawImage(
            !getHover() ? stopIcon : stopIconHover,
            (int) origin.getX(),
            (int) origin.getY(),
            null
        );
    }

    /**
     * Get the stop icon
     *
     * @return the stop icon
     */
    private BufferedImage getStopIcon()
    {
        try {
            return ImageIO.read( getClass().getResource( "/res/img/btn-stop.png" ) );
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Get the stop icon for hover state
     *
     * @return the stop icon for hover state
     */
    private BufferedImage getStopIconHover()
    {
        try {
            return ImageIO.read( getClass().getResource( "/res/img/btn-stop-hover.png" ) );
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        return null;
    }
}
