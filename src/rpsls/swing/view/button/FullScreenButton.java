package rpsls.swing.view.button;

import rpsls.core.util.Dimension;
import rpsls.core.util.Position;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * A button to trigger a fullscreen state toggle
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class FullScreenButton extends ControlButton
{
    private BufferedImage icon, iconHover;

    /**
     * FullScreenButton constructor
     */
    public FullScreenButton()
    {
        super();

        icon = getIcon();
        iconHover = getIconHover();
    }

    @Override
    public void draw( Graphics2D g2d, Position origin, Dimension parentDimension )
    {
        g2d.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
        g2d.drawImage(
            !getHover() ? icon : iconHover,
            (int) origin.getX(),
            (int) origin.getY(),
            null
        );
    }
    /**
     * Get the icon of the button
     *
     * @return the icon of the button
     */
    private BufferedImage getIcon()
    {
        try {
            return ImageIO.read( getClass().getResource( "/res/img/btn-full-screen.png" ) );
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Get the icon of the button for hover state
     *
     * @return the icon of the button for hover state
     */
    private BufferedImage getIconHover()
    {
        try {
            return ImageIO.read( getClass().getResource( "/res/img/btn-full-screen-hover.png" ) );
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        return null;
    }
}
