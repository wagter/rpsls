package rpsls.swing.view.button;

import rpsls.core.state.GameState;
import rpsls.core.util.Dimension;
import rpsls.core.util.Position;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * A button to toggle the sound state of the game
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class MuteButton extends ControlButton
{
    private BufferedImage muteIcon, muteIconHover, unmuteIcon, unmuteIconHover;
    private GameState gameState;

    /**
     * PlayButton constructor
     *
     * @param gameState to change the button's image based on the play/pause state
     */
    public MuteButton( GameState gameState )
    {
        super();

        this.gameState = gameState;

        this.muteIcon = getMuteIcon();
        this.muteIconHover = getMuteIconHover();
        this.unmuteIcon = getUnmuteIcon();
        this.unmuteIconHover = getUnmuteIconHover();
    }

    @Override
    public void draw( Graphics2D g2d, Position origin, Dimension parentDimension )
    {
        g2d.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
        g2d.drawImage(
            gameState.isMuted()
                ? getHover() ? unmuteIconHover : unmuteIcon
                : getHover() ? muteIconHover : muteIcon,
            (int) origin.getX(),
            (int) origin.getY(),
            null
        );
    }

    /**
     * Get the mute icon
     *
     * @return the mute icon
     */
    private BufferedImage getMuteIcon()
    {
        try {
            return ImageIO.read( getClass().getResource( "/res/img/btn-mute.png" ) );
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Get the mute icon for hover state
     *
     * @return the mute icon for hover state
     */
    private BufferedImage getMuteIconHover()
    {
        try {
            return ImageIO.read( getClass().getResource( "/res/img/btn-mute-hover.png" ) );
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Get the unmute icon
     *
     * @return the unmute icon
     */
    private BufferedImage getUnmuteIcon()
    {
        try {
            return ImageIO.read( getClass().getResource( "/res/img/btn-unmute.png" ) );
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Get the unmute icon for hover state
     *
     * @return the unmute icon for hover state
     */
    private BufferedImage getUnmuteIconHover()
    {
        try {
            return ImageIO.read( getClass().getResource( "/res/img/btn-unmute-hover.png" ) );
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        return null;
    }
}
