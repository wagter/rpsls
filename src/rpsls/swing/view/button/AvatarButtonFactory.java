package rpsls.swing.view.button;

import rpsls.core.avatar.Avatar;

import java.awt.*;
import java.util.List;

/**
 * Factory class to create AvatarButton instances
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class AvatarButtonFactory
{
    /**
     * Create a list of AvatarButton instances for a list of Avatar instances
     *
     * @param avatars  the list of Avatar instances
     * @return the list of AvatarButton instances
     */
    public static AvatarButtonList create( List<Avatar> avatars, Font font )
    {
        return new AvatarButtonList()
        {{
            for ( Avatar avatar : avatars ) {
                add( create( avatar, font ) );
            }
        }};
    }

    /**
     * Create an AvatarButton instance for a single Avatar instance
     *
     * @param avatar the Avatar instance
     * @return the AvatarButton instance
     */
    private static AvatarButton create( Avatar avatar, Font font )
    {
        return new AvatarButton( avatar, font );
    }
}
