package rpsls.swing.view.button;

import rpsls.core.state.GameState;
import rpsls.core.util.Dimension;
import rpsls.core.util.Position;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * A button to toggle the play/pause state of the game
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class PlayButton extends ControlButton
{
    private BufferedImage playIcon, playIconHover, pauseIcon, pauseIconHover;
    private GameState gameState;

    /**
     * PlayButton constructor
     *
     * @param gameState to change the button's image based on the play/pause state
     */
    public PlayButton( GameState gameState )
    {
        super();

        this.gameState = gameState;

        this.playIcon = getPlayIcon();
        this.playIconHover = getPlayIconHover();
        this.pauseIcon = getPauseIcon();
        this.pauseIconHover = getPauseIconHover();
    }

    @Override
    public void draw( Graphics2D g2d, Position origin, Dimension parentDimension )
    {
        g2d.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
        g2d.drawImage(
            gameState.isPaused()
                ? getHover() ? playIconHover : playIcon
                : getHover() ? pauseIconHover : pauseIcon,
            (int) origin.getX(),
            (int) origin.getY(),
            null
        );
    }

    /**
     * Get the play icon
     *
     * @return the play icon
     */
    private BufferedImage getPlayIcon()
    {
        try {
            return ImageIO.read( getClass().getResource( "/res/img/btn-play.png" ) );
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Get the play icon for hover state
     *
     * @return the play icon for hover state
     */
    private BufferedImage getPlayIconHover()
    {
        try {
            return ImageIO.read( getClass().getResource( "/res/img/btn-play-hover.png" ) );
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Get the pause icon
     *
     * @return the pause icon
     */
    private BufferedImage getPauseIcon()
    {
        try {
            return ImageIO.read( getClass().getResource( "/res/img/btn-pause.png" ) );
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Get the pause icon for hover state
     *
     * @return the pause icon for hover state
     */
    private BufferedImage getPauseIconHover()
    {
        try {
            return ImageIO.read( getClass().getResource( "/res/img/btn-pause-hover.png" ) );
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        return null;
    }
}
