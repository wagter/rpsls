package rpsls.swing.view.button;

import rpsls.core.util.Dimension;
import rpsls.core.util.Position;

import rpsls.swing.view.drawable.Drawable;
import rpsls.swing.view.drawable.Hoverable;

import javax.swing.*;
import java.awt.*;

/**
 * Abstract button to extend for making action buttons
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public abstract class ControlButton extends JComponent implements Hoverable, Drawable
{
    private boolean hover = false;

    /**
     * ControlButton constructor
     */
    public ControlButton()
    {
        setPreferredSize( new java.awt.Dimension( 40, 32 ) );
    }

    @Override
    public void paintComponent( Graphics g )
    {
        draw( (Graphics2D) g );
    }

    @Override
    public boolean getHover()
    {
        return hover;
    }

    @Override
    public void setHover( boolean hover )
    {
        this.hover = hover;
    }

    @Override
    public void draw( Graphics2D g2d )
    {
        draw( g2d, new Position( 0, 0 ) );
    }

    @Override
    public void draw( Graphics2D g2d, Position origin )
    {
        draw( g2d, origin, new Dimension( 0, 0 ) );
    }
}
