package rpsls.swing.view;

import javax.swing.*;
import java.awt.*;

/**
 * View for displaying the top bar
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class TopBarView extends BasePanelView
{
    /**
     * TopBarView constructor
     *
     * @param hintsView the view for displaying hints
     * @param playButton the view for displaying the control buttons
     */
    public TopBarView( HintsView hintsView, JComponent playButton )
    {
        super();
        add( hintsView, BorderLayout.CENTER );
        add( playButton, BorderLayout.EAST );
    }

    @Override
    protected void initLayout()
    {
        setLayout( new BorderLayout() );
        setBackground( Color.decode( "#303030" ) );
        setBorder( BorderFactory.createEmptyBorder( 16, 16, 16, 16 ) );
    }
}
