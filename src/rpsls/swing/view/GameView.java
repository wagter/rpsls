package rpsls.swing.view;

import java.awt.*;

/**
 * The main view class
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class GameView extends BasePanelView
{
    /**
     * GameView constructor
     *
     * @param viewportView the viewport object
     * @param avatarButtonsView the avatar buttons object
     * @param sideBarView the sidebar object
     * @param topBarView the top bar object
     */
    public GameView(
        ViewportView viewportView,
        AvatarButtonsView avatarButtonsView,
        SideBarView sideBarView,
        TopBarView topBarView
    )
    {
        super();
        add( topBarView, BorderLayout.NORTH );
        add( viewportView, BorderLayout.CENTER );
        add( avatarButtonsView, BorderLayout.SOUTH );
        add( sideBarView, BorderLayout.EAST );
    }

    @Override
    protected void initLayout()
    {
        setLayout( new BorderLayout() );
        setBackground( Color.decode( "#212121" ) );
    }
}
