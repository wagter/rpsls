package rpsls.swing.view;

import rpsls.swing.view.button.AvatarButton;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * View for multiple AvatarButton instances
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class AvatarButtonsView extends BasePanelView
{
    /**
     * AvatarButtonsView constructor
     *
     * @param avatarButtons the list of AvatarButton instances
     */
    public AvatarButtonsView( List<AvatarButton> avatarButtons )
    {
        super();

        for ( AvatarButton avatarButton : avatarButtons ) {
            add( avatarButton );
        }
    }

    @Override
    protected void initLayout()
    {
        setBackground( Color.decode( "#303030" ) );
        setBorder( BorderFactory.createEmptyBorder( 16, 16, 16, 16 ) );
    }
}
