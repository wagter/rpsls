package rpsls.swing.audio;

import rpsls.core.audio.AudioPlayer;
import rpsls.core.state.GameState;

import javax.sound.sampled.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * AudioPlayer implementation for Java Swing
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class AudioPlayerImpl implements AudioPlayer
{
    private Map<String, Clip> clipMap = new HashMap<>();
    private Map<String, Boolean> isPlayingMap = new HashMap<>();
    private GameState gameState;
    private String basePath;

    public AudioPlayerImpl(
        GameState gameState,
        String basePath,
        Map<String, String> filePathMap
    )
    {
        this.gameState = gameState;
        this.basePath = basePath;
        preloadClips( filePathMap );
    }

    @Override
    public void play( String file )
    {
        play( file, false );
    }

    @Override
    public void play( String file, boolean rewind )
    {
        if ( rewind ) {
            rewind( file );
        }

        Clip clip = getClip( file );
        if ( clip == null) {
            return;
        }

        new Thread( () -> {
            FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);

            double gain = gameState.isMuted() ? 0 : 0.25;
            float dB = (float) (Math.log(gain) / Math.log(10.0) * 20.0);
            gainControl.setValue(dB);
            clip.start();
        } ).start();

        isPlayingMap.put( file, true );
    }

    @Override
    public void stop( String file )
    {
        Clip clip = getClip( file );
        if (clip != null) {
            clip.stop();
        }
        isPlayingMap.put( file, false );
    }

    @Override
    public void rewind( String file )
    {
        Clip clip = getClip( file );
        if (clip != null) {
            clip.setMicrosecondPosition( 0 );
        }
    }

    @Override
    public boolean isPlaying( String file )
    {
        return isPlayingMap.containsKey(file ) && isPlayingMap.get( file );
    }

    /**
     * Get audio clip from a certain audio file
     *
     * @param fileName the path to the audio file
     * @return the clip for the audio file
     */
    private Clip getClip( String fileName )
    {
        if ( clipMap.containsKey( fileName ) ) {
            return clipMap.get( fileName );
        }

        return null;
    }

    /**
     * Preload multiple audio clips
     *
     * @param filePathMap the map of names and file paths
     */
    private void preloadClips( Map<String, String> filePathMap )
    {
        for (Map.Entry<String, String> entry : filePathMap.entrySet()) {
            try {
                preloadClip( entry.getKey(), entry.getValue() );
            } catch ( LineUnavailableException | IOException | UnsupportedAudioFileException e ) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Preload an audio clip
     *
     * @param clipName the name of the clip
     * @throws LineUnavailableException if a line is unavailable
     * @throws IOException if the file cannot be found
     * @throws UnsupportedAudioFileException if the file extension is unsupported
     */
    private void preloadClip( String clipName, String fileName ) throws IOException, UnsupportedAudioFileException, LineUnavailableException
    {
        AudioInputStream stream = AudioSystem.getAudioInputStream( this.getClass().getResource( basePath + "/" + fileName ) );
        AudioFormat format = stream.getFormat();

        DataLine.Info info = new DataLine.Info( Clip.class, format );
        Clip clip = (Clip) AudioSystem.getLine( info );
        clip.open( stream );

        clipMap.put( clipName, clip );
    }
}
