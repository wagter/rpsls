package rpsls.swing.audio;

import rpsls.core.audio.AudioPlayer;
import rpsls.core.state.GameState;

import java.util.Map;

public class AudioPlayerFactory
{
    public static AudioPlayer create( GameState gameState, String basePath, Map<String, String> filePathMap )
    {
        return new AudioPlayerImpl( gameState, basePath, filePathMap );
    }
}
