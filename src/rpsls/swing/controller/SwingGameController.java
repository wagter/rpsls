package rpsls.swing.controller;

import rpsls.core.audio.AudioPlayer;
import rpsls.core.controller.GameController;
import rpsls.core.score.ScoreBoard;
import rpsls.core.spawner.Spawner;
import rpsls.core.state.GameState;

import java.awt.*;

/**
 * The game controller for the Swing implementation
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class SwingGameController extends GameController
{
    /**
     * SwingGameController constructor
     *
     * @param gameState the GameState instance
     * @param spawner the Spawner instance
     * @param audioPlayer the AudioPlayer instance
     * @param scoreBoard the ScoreBoard instance
     */
    public SwingGameController(
        GameState gameState,
        Spawner spawner,
        AudioPlayer audioPlayer,
        ScoreBoard scoreBoard
    ) {
        super( gameState, spawner, audioPlayer, scoreBoard );
    }


    @Override
    public void toggleFullScreenAction()
    {
        audioPlayer.play( "fullscreen", true );
        gameState.setPaused( true );
        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice device = env.getDefaultScreenDevice();

        if ( !gameState.isFullScreen() ) {
            gameState.setFullScreen( true );
            for (Frame frame : Frame.getFrames()) {
                if (frame.isVisible()) {
                    device.setFullScreenWindow( frame );
                }
            }
            return;
        }

        gameState.setFullScreen( false );
        device.setFullScreenWindow( null );
    }
}
