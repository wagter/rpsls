package rpsls.swing.adapter;

import rpsls.core.controller.GameController;

import javax.swing.event.MouseInputAdapter;
import java.awt.event.MouseEvent;

/**
 * Mouse input adapter to trigger a stop rpsls.core action
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class StopGameAdapter extends MouseInputAdapter
{
    private GameController controller;

    /**
     * StopGameAdapter constructor
     *
     * @param controller the GameController instance
     */
    public StopGameAdapter( GameController controller )
    {
        this.controller = controller;
    }

    @Override
    public void mousePressed( MouseEvent me )
    {
        controller.stopAction();
    }
}
