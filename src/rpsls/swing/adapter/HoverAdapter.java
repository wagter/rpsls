package rpsls.swing.adapter;

import rpsls.swing.view.drawable.Hoverable;

import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * Mouse input adapter to set hover state on Hoverable elements and set cursor on frame
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class HoverAdapter extends MouseInputAdapter
{
    private Cursor defaultCursor, hoverCursor;

    /**
     * HoverAdapter constructor
     */
    public HoverAdapter()
    {
        this.defaultCursor = new Cursor( Cursor.DEFAULT_CURSOR );
        this.hoverCursor =  new Cursor( Cursor.HAND_CURSOR );
    }

    @Override
    public void mouseEntered( MouseEvent me )
    {
        Hoverable hoverable = getHoverable( me );
        hoverable.setHover( true );
        setCursor( hoverCursor );
    }

    @Override
    public void mouseExited( MouseEvent me )
    {
        Hoverable hoverable = getHoverable( me );
        hoverable.setHover( false );
        setCursor( defaultCursor );
    }

    /**
     * Get the Hoverable object
     *
     * @param me the MouseEvent object
     * @return the Hoverable object
     */
    private Hoverable getHoverable( MouseEvent me )
    {
        return (Hoverable) me.getSource();
    }

    /**
     * Set cursor to frames
     *
     * @param cursor the Cursor instance
     */
    private void setCursor( Cursor cursor )
    {
        for ( Frame frame : Frame.getFrames() ) {
            if ( frame.isVisible() ) {
                frame.setCursor( cursor );
            }
        }
    }
}
