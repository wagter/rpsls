package rpsls.swing.adapter;

import rpsls.core.avatar.Avatar;
import rpsls.core.controller.AvatarController;
import rpsls.swing.view.button.AvatarButton;

import javax.swing.event.MouseInputAdapter;
import java.awt.event.MouseEvent;

/**
 * Mouse input adapter to trigger a beat avatar action
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class BeatAvatarAdapter extends MouseInputAdapter
{
    private AvatarController avatarController;

    /**
     * BeatAvatarAdapter constructor
     *
     * @param avatarController the AvatarController instance
     */
    public BeatAvatarAdapter( AvatarController avatarController )
    {
        this.avatarController = avatarController;
    }

    @Override
    public void mousePressed( MouseEvent me )
    {
        AvatarButton btn = (AvatarButton) me.getSource();
        Avatar beats = btn.getAvatar();
        avatarController.beatAction( beats );
    }
}
