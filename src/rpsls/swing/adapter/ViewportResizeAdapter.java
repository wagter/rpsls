package rpsls.swing.adapter;

import rpsls.core.state.GameState;
import rpsls.core.util.Dimension;

import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/**
 * Component adapter to update the viewport size
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class ViewportResizeAdapter extends ComponentAdapter
{
    private GameState gameState;

    /**
     * ViewportResizeAdapter constructor
     *
     * @param gameState the GameState instance
     */
    public ViewportResizeAdapter( GameState gameState )
    {
        this.gameState = gameState;
    }

    @Override
    public void componentResized( ComponentEvent componentEvent)
    {
        Component viewport = componentEvent.getComponent();
        gameState.setViewportDimension(
            new Dimension(
                viewport.getWidth(),
                viewport.getHeight()
            )
        );
    }
}
