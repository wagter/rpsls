package rpsls.swing.adapter;

import rpsls.core.controller.GameController;

import javax.swing.event.MouseInputAdapter;
import java.awt.event.MouseEvent;

/**
 * Mouse input adapter to trigger a toggle fullscreen action
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class ToggleFullScreenAdapter extends MouseInputAdapter
{
    private GameController controller;

    /**
     * ToggleFullScreenAdapter constructor
     *
     * @param controller the GameController instance
     */
    public ToggleFullScreenAdapter( GameController controller )
    {
        this.controller = controller;
    }

    @Override
    public void mousePressed( MouseEvent me )
    {
        controller.toggleFullScreenAction();
    }
}
