package rpsls.swing.adapter;

import rpsls.core.controller.GameController;

import javax.swing.event.MouseInputAdapter;
import java.awt.event.MouseEvent;

/**
 * Mouse input adapter to set the game's play/pause state
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class PlayPauseAdapter extends MouseInputAdapter
{
    private GameController gameController;

    /**
     * PlayPauseAdapter constructor
     *
     * @param gameController the GameController instance
     */
    public PlayPauseAdapter( GameController gameController )
    {
        this.gameController = gameController;
    }

    @Override
    public void mousePressed( MouseEvent me )
    {
        gameController.togglePlayAction();
    }
}
