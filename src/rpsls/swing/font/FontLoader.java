package rpsls.swing.font;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.io.InputStream;

/**
 * Class to load web fonts
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class FontLoader
{
    /**
     * Load a web font
     *
     * @param path the path to the web font
     * @return the loaded Font instance, default font if requested font is not found or invalid.
     */
    public Font loadFont( String path )
    {
        try {
            InputStream fontIs = getClass().getResourceAsStream( path );
            return Font.createFont( Font.TRUETYPE_FONT, fontIs );
        } catch ( FontFormatException | IOException e ) {
            return new JLabel().getFont();
        }
    }
}
