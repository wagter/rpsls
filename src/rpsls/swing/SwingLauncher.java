package rpsls.swing;

import rpsls.core.launcher.CoreLauncher;
import rpsls.core.loop.GameLoopRunner;

import javax.swing.*;

public class SwingLauncher extends CoreLauncher
{
    private JFrame frame;

    public SwingLauncher( GameLoopRunner loopRunner, JFrame frame )
    {
        super( loopRunner );
        this.frame = frame;
    }

    @Override
    public void launch()
    {
        super.launch();
        frame.setVisible( true );
    }
}
