package rpsls.swing.container;

import rpsls.core.audio.AudioPlayer;
import rpsls.core.avatar.AvatarList;
import rpsls.core.container.Container;
import rpsls.core.container.ContainerConfigurator;
import rpsls.core.controller.AvatarController;
import rpsls.core.controller.GameController;
import rpsls.core.hint.Hints;
import rpsls.core.launcher.GameLauncher;
import rpsls.core.loop.GameLoopRunner;
import rpsls.core.score.ScoreBoard;
import rpsls.core.spawner.Spawner;
import rpsls.core.state.GameState;
import rpsls.core.view.View;

import rpsls.swing.FrameFactory;
import rpsls.swing.SwingLauncher;
import rpsls.swing.adapter.*;
import rpsls.swing.audio.AudioPlayerFactory;
import rpsls.swing.controller.SwingGameController;
import rpsls.swing.font.FontLoader;
import rpsls.swing.view.*;
import rpsls.swing.view.button.*;
import rpsls.swing.view.drawable.AvatarDrawableFactory;
import rpsls.swing.view.drawable.DrawableArray;
import rpsls.swing.view.drawable.PauseDrawable;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

public class SwingContainerConfigurator implements ContainerConfigurator
{
    @Override
    public void configure( Container container )
    {

        container

            .setParameter( "color_bg_lt", "#303030" )
            .setParameter( "color_bg_dk", "#212121" )

            .setParameter( "path_audio", "/res/audio" )
            .setParameter( "path_img", "/res/img" )
            .setParameter( "path_font", "/res/font" )

            // override AudioPlayer because it
            // has no implementation in the core
            .setResolver(
                AudioPlayer.class,
                () -> AudioPlayerFactory.create(
                    container.get( GameState.class ),
                    container.getParameter( "path_audio" ),
                    new HashMap<String, String>()
                    {{
                        put( "beat", "kick-short.wav" );
                        put( "wrong", "bump-short.wav" );
                        put( "spawn", "thwomp-short.wav" );
                        put( "pause", "pause.wav" );
                        put( "fullscreen", "warp.wav" );
                    }}
                )
            )

            // override GameController because the toggleFullscreen
            // action has no implementation in the core
            .setResolver(
                GameController.class,
                () -> new SwingGameController(
                    container.get( GameState.class ),
                    container.get( Spawner.class ),
                    container.get( AudioPlayer.class ),
                    container.get( ScoreBoard.class )
                )
            )

            // override GameLauncher to launch the Swing implementation
            .setResolver(
                GameLauncher.class,
                () -> new SwingLauncher(
                    container.get( GameLoopRunner.class ),
                    container.get( JFrame.class )
                )
            )

            // configure adapters
            .setResolver(
                BeatAvatarAdapter.class,
                () -> new BeatAvatarAdapter( container.get( AvatarController.class ) )
            )
            .setResolver(
                HoverAdapter.class,
                HoverAdapter::new
            )
            .setResolver(
                ViewportResizeAdapter.class,
                () -> new ViewportResizeAdapter( container.get( GameState.class ) )
            )
            .setResolver(
                ToggleFullScreenAdapter.class,
                () -> new ToggleFullScreenAdapter( container.get( GameController.class ) )
            )
            .setResolver(
                PlayPauseAdapter.class,
                () -> new PlayPauseAdapter( container.get( GameController.class ) )
            )
            .setResolver(
                StopGameAdapter.class,
                () -> new StopGameAdapter( container.get( GameController.class ) )
            )
            .setResolver(
                ToggleSoundAdapter.class,
                () -> new ToggleSoundAdapter( container.get( GameController.class ) )
            )

            // configure buttons
            .setResolver(
                AvatarButtonList.class,
                () -> {
                    AvatarButtonList avatarButtons = AvatarButtonFactory.create(
                        container.get( AvatarList.class ),
                        container.get( Font.class )
                    );
                    for ( AvatarButton avatarButton : avatarButtons ) {
                        avatarButton.addMouseListener( container.get( HoverAdapter.class ) );
                        avatarButton.addMouseListener( container.get( BeatAvatarAdapter.class ) );
                    }
                    return avatarButtons;
                }
            )
            .setResolver(
                PlayButton.class,
                () -> {
                    PlayButton playButton = new PlayButton( container.get( GameState.class ) );
                    playButton.addMouseListener( container.get( PlayPauseAdapter.class ) );
                    playButton.addMouseListener( container.get( HoverAdapter.class ) );
                    return playButton;
                }
            )
            .setResolver(
                StopButton.class,
                () -> {
                    StopButton stopButton = new StopButton();
                    stopButton.addMouseListener( container.get( StopGameAdapter.class ) );
                    stopButton.addMouseListener( container.get( HoverAdapter.class ) );
                    return stopButton;
                }
            )
            .setResolver(
                FullScreenButton.class,
                () -> {
                    FullScreenButton fullScreenButton = new FullScreenButton();
                    fullScreenButton.addMouseListener( container.get( ToggleFullScreenAdapter.class ) );
                    fullScreenButton.addMouseListener( container.get( HoverAdapter.class ) );
                    return fullScreenButton;
                }
            )
            .setResolver(
                MuteButton.class,
                () -> {
                    MuteButton muteButton = new MuteButton( container.get( GameState.class ) );
                    muteButton.addMouseListener( container.get( ToggleSoundAdapter.class ) );
                    muteButton.addMouseListener( container.get( HoverAdapter.class ) );
                    return muteButton;
                }
            )

            // configure views
            .setResolver(
                JFrame.class,
                () -> {
                    JFrame frame = FrameFactory.createRegularFrame();
                    frame.setContentPane( container.get( GameView.class ) );
                    return frame;
                }
            )
            .setResolver(
                View.class,
                () -> container.get( GameView.class )
            )
            .setResolver(
                GameView.class,
                () -> new GameView(
                    container.get( ViewportView.class ),
                    container.get( AvatarButtonsView.class ),
                    container.get( SideBarView.class ),
                    container.get( TopBarView.class )
                )
            )
            .setResolver(
                AvatarButtonsView.class,
                () -> new AvatarButtonsView( container.get( AvatarButtonList.class ) )
            )
            .setResolver(
                ControlButtonView.class,
                () -> new ControlButtonView( new ArrayList<ControlButton>()
                {{
                    add( container.get( PlayButton.class ) );
                    add( container.get( StopButton.class ) );
                    add( container.get( MuteButton.class ) );
                    add( container.get( FullScreenButton.class ) );
                }} )
            )
            .setResolver(
                ViewportView.class,
                () -> {
                    DrawableArray avatarDrawables = AvatarDrawableFactory.create(
                        container.get( AvatarList.class )
                    );
                    avatarDrawables.add( container.get( PauseDrawable.class ) );
                    ViewportView viewportView = new ViewportView( avatarDrawables );
                    viewportView.addComponentListener( container.get( ViewportResizeAdapter.class ) );
                    return viewportView;
                }
            )
            .setResolver(
                PauseDrawable.class,
                () -> new PauseDrawable( container.get( GameState.class ) )
            )
            .setResolver(
                HintsView.class,
                () -> new HintsView(
                    container.get( Hints.class ),
                    container.get( Font.class )
                )
            )
            .setResolver(
                TopBarView.class,
                () -> new TopBarView(
                    container.get( HintsView.class ),
                    container.get( ControlButtonView.class )
                )
            )
            .setResolver(
                ScoreBoardView.class,
                () -> new ScoreBoardView(
                    container.get( ScoreBoard.class ),
                    container.get( Font.class )
                )
            )
            .setResolver(
                SideBarView.class,
                () -> new SideBarView( container.get( ScoreBoardView.class ) )
            )
            .setResolver(
                Font.class,
                () -> new FontLoader().loadFont( "/res/font/Righteous-Regular.ttf" )
            )
        ;
    }
}