package rpsls.core.beater;

import rpsls.core.avatar.Avatar;

import java.util.List;

/**
 * AvatarBeater implementation
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class AvatarBeaterImpl implements AvatarBeater
{
    private List<Avatar> avatars;

    /**
     * AvatarBeaterImpl constructor
     *
     * @param avatars the list of Avatar instances
     */
    public AvatarBeaterImpl( List<Avatar> avatars )
    {
        this.avatars = avatars;
    }

    @Override
    public Long beat( Avatar beats )
    {
        for ( Avatar avatar : avatars ) {
            if ( avatar.isSpawned() && beats.beats( avatar ) ) {
                avatar.setSpawned( false );
                return System.currentTimeMillis() - avatar.getLastSpawned();
            }
        }

        return null;
    }
}
