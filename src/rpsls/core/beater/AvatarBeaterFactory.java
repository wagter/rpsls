package rpsls.core.beater;

import rpsls.core.avatar.Avatar;

import java.util.List;

/**
 * Factory class to create AvatarBeater instances
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class AvatarBeaterFactory
{
    /**
     * Create an AvatarBeater instance based on a list of Avatar instances
     *
     * @param avatars the list of Avatar instances
     * @return the AvatarBeater instance
     */
    public static AvatarBeater create( List<Avatar> avatars )
    {
        return new AvatarBeaterImpl( avatars );
    }
}
