package rpsls.core.beater;

import rpsls.core.avatar.Avatar;

/**
 * Defining an Avatar beater service
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface AvatarBeater
{
    /**
     * Beat an Avatar
     *
     * @param beats the beating Avatar
     * @return the score, or null if wrong
     */
    Long beat( Avatar beats );
}
