package rpsls.core.container;

/**
 * A class to configure a Container instance
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface ContainerConfigurator
{
    /**
     * Configure the Container instance
     *
     * @param container the Container instance to configure
     */
    void configure( Container container );
}
