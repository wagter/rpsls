package rpsls.core.container;

import java.util.HashMap;
import java.util.Map;

/**
 * Container implementation
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class ContainerImpl implements Container
{
    private Map<Class, Object> services = new HashMap<>();
    private Map<Class, ServiceResolver> resolvers = new HashMap<>();
    private Map<String, String> parameters = new HashMap<>();

    @Override
    @SuppressWarnings("unchecked")
    public <T> T get( Class<T> serviceClass ) throws UnresolvableServiceException, UnknownParameterKeyException
    {
        if ( !isResolved( serviceClass ) ) {
            resolve( serviceClass );
        }

        return (T) services.get( serviceClass );
    }

    @Override
    public void set( Class serviceClass, Object service )
    {
        services.put( serviceClass, service );
    }

    @Override
    public Container setParameter( String key, String value )
    {
        parameters.put( key, value );
        return this;
    }

    @Override
    public String getParameter( String key ) throws UnknownParameterKeyException
    {
        if (!parameters.containsKey( key )) {
            throw new UnknownParameterKeyException( key );
        }

        return parameters.get( key );
    }

    @Override
    public Container setResolver( Class serviceClass, ServiceResolver serviceResolver )
    {
        resolvers.put( serviceClass, serviceResolver );

        return this;
    }

    @Override
    public void resolve( Class serviceClass ) throws UnresolvableServiceException, UnknownParameterKeyException
    {
        if ( !resolvers.containsKey( serviceClass ) ) {
            throw new UnresolvableServiceException( serviceClass );
        }

        set( serviceClass, resolvers.get( serviceClass ).resolve() );
    }

    @Override
    public boolean isResolved( Class serviceClass )
    {
        return services.containsKey( serviceClass );
    }

    @Override
    public void configure( ContainerConfigurator configurator )
    {
        configurator.configure( this );
    }
}
