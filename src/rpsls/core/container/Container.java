package rpsls.core.container;

/**
 * A container to keep objects that are needed throughout the application
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface Container
{
    /**
     * Get a service from the container
     *
     * @param serviceClass the class of the service to get from the container
     * @param <T>          the type of the class of the service to get from the container
     * @return the service from the container
     * @throws UnresolvableServiceException if the service is not resolvable (undefined)
     */
    <T> T get( Class<T> serviceClass ) throws UnresolvableServiceException, UnknownParameterKeyException;

    /**
     * Set a service to the container
     *
     * @param serviceClass the class of the service
     * @param service      the service
     */
    void set( Class serviceClass, Object service );

    /**
     * Set a parameter to the container
     *
     * @param key   the parameter key
     * @param value the parameter value
     * @return self
     */
    Container setParameter( String key, String value );

    /**
     * Get a parameter from the container
     *
     * @param key the parameter key
     * @return the parameter value
     */
    String getParameter( String key ) throws UnknownParameterKeyException;

    /**
     * Set a service resolver to the container
     *
     * @param serviceClass    the class of the service to resolve
     * @param serviceResolver the ServiceResolver instance
     * @return self
     */
    Container setResolver( Class serviceClass, ServiceResolver serviceResolver );

    /**
     * Resolve a service
     *
     * @param serviceClass the class of the service to resolve
     * @throws UnresolvableServiceException if the service is not resolvable (undefined)
     */
    void resolve( Class serviceClass ) throws UnresolvableServiceException, UnknownParameterKeyException;

    /**
     * Check if a service is resolved
     *
     * @param serviceClass the class of the service to check
     * @return true if the service is resolved
     */
    boolean isResolved( Class serviceClass );

    /**
     * Configure the container
     *
     * @param configurator the ContainerConfigurator instance
     */
    void configure( ContainerConfigurator configurator );
}
