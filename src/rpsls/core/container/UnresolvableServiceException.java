package rpsls.core.container;

/**
 * To be thrown when a requested service cannot be resolved
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class UnresolvableServiceException extends Exception
{
    /**
     * UnresolvableServiceException constructor
     *
     * @param serviceClass the class of the requested service
     */
    UnresolvableServiceException( Class serviceClass )
    {
        super( "The requested service: \"" + serviceClass.toString() + "\" is unresolvable." );
    }
}
