package rpsls.core.container;

public interface ServiceResolver
{
    Object resolve() throws UnresolvableServiceException, UnknownParameterKeyException;
}
