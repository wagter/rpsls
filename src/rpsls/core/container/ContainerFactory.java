package rpsls.core.container;

/**
 * Factory class to create Container instances
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class ContainerFactory
{
    /**
     * Create a Container instance with the core already configured
     *
     * @return the configured Container instance
     */
    public static Container createCoreContainer()
    {
        Container container = create();
        container.configure( new CoreContainerConfigurator() );

        return container;
    }

    /**
     * Create an empty container
     *
     * @return the empty Container instance
     */
    public static Container create()
    {
        return new ContainerImpl();
    }
}
