package rpsls.core.container;

public class UnknownParameterKeyException extends Exception
{
    public UnknownParameterKeyException( String parameterKey )
    {
        super("The key " + parameterKey + " is not a known parameter");
    }
}
