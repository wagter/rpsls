package rpsls.core.container;

import rpsls.core.audio.AudioPlayer;
import rpsls.core.audio.DummyPlayer;
import rpsls.core.avatar.*;
import rpsls.core.beater.AvatarBeater;
import rpsls.core.beater.AvatarBeaterFactory;
import rpsls.core.controller.AvatarController;
import rpsls.core.controller.GameController;
import rpsls.core.fader.AvatarFader;
import rpsls.core.fader.AvatarFaderFactory;
import rpsls.core.hint.Hints;
import rpsls.core.hint.HintsBuilder;
import rpsls.core.launcher.CoreLauncher;
import rpsls.core.launcher.GameLauncher;
import rpsls.core.loop.GameLoop;
import rpsls.core.loop.GameLoopImpl;
import rpsls.core.loop.GameLoopRunner;
import rpsls.core.loop.GameLoopRunnerImpl;
import rpsls.core.score.AvatarScoreFactory;
import rpsls.core.score.ScoreBoard;
import rpsls.core.spawner.Spawner;
import rpsls.core.spawner.SpawnerFactory;
import rpsls.core.state.GameState;
import rpsls.core.view.DummyView;
import rpsls.core.view.View;
import rpsls.core.walker.AvatarWalker;
import rpsls.core.walker.AvatarWalkerFactory;

public class CoreContainerConfigurator implements ContainerConfigurator
{
    @Override
    public void configure( Container container )
    {
        container

            // configure parameters
            .setParameter( "color_rock", "#ffd5d5" )
            .setParameter( "color_paper", "#fff6d5" )
            .setParameter( "color_scissors", "#f6d5ff" )
            .setParameter( "color_lizard", "#d7f4d7" )
            .setParameter( "color_spock", "#d5e5ff" )

            // configure services
            .setResolver(
                RockAvatar.class,
                () -> new RockAvatar( container.getParameter( "color_rock" ) )
            )
            .setResolver(
                PaperAvatar.class,
                () -> new PaperAvatar( container.getParameter( "color_paper" ) )
            )
            .setResolver(
                ScissorsAvatar.class,
                () -> new ScissorsAvatar( container.getParameter( "color_scissors" ) )
            )
            .setResolver(
                LizardAvatar.class,
                () -> new LizardAvatar( container.getParameter( "color_lizard" ) )
            )
            .setResolver(
                SpockAvatar.class,
                () -> new SpockAvatar( container.getParameter( "color_spock" ) )
            )

            .setResolver( AvatarList.class, () -> {
                AvatarList avatarList = new AvatarListImpl();
                avatarList.add( container.get( RockAvatar.class ) );
                avatarList.add( container.get( PaperAvatar.class ) );
                avatarList.add( container.get( ScissorsAvatar.class ) );
                avatarList.add( container.get( LizardAvatar.class ) );
                avatarList.add( container.get( SpockAvatar.class ) );
                return avatarList;
            } )

            .setResolver( Hints.class, () -> {
                HintsBuilder hintsBuilder = new HintsBuilder();
                hintsBuilder.add(
                    container.get( RockAvatar.class ),
                    container.get( ScissorsAvatar.class ),
                    "crushes"
                );
                hintsBuilder.add(
                    container.get( RockAvatar.class ),
                    container.get( LizardAvatar.class ),
                    "crushes"
                );
                hintsBuilder.add(
                    container.get( PaperAvatar.class ),
                    container.get( RockAvatar.class ),
                    "covers"
                );
                hintsBuilder.add(
                    container.get( PaperAvatar.class ),
                    container.get( SpockAvatar.class ),
                    "disproves"
                );
                hintsBuilder.add(
                    container.get( ScissorsAvatar.class ),
                    container.get( PaperAvatar.class ),
                    "cuts"
                );
                hintsBuilder.add(
                    container.get( ScissorsAvatar.class ),
                    container.get( LizardAvatar.class ),
                    "decapitates"
                );
                hintsBuilder.add(
                    container.get( LizardAvatar.class ),
                    container.get( PaperAvatar.class ),
                    "eats"
                );
                hintsBuilder.add(
                    container.get( LizardAvatar.class ),
                    container.get( SpockAvatar.class ),
                    "poisons"
                );
                hintsBuilder.add(
                    container.get( SpockAvatar.class ),
                    container.get( ScissorsAvatar.class ),
                    "smashes"
                );
                hintsBuilder.add(
                    container.get( SpockAvatar.class ),
                    container.get( RockAvatar.class ),
                    "vaporizes"
                );
                return hintsBuilder.build();
            } )

            .setResolver(
                GameState.class,
                GameState::new
            )
            .setResolver(
                Spawner.class, () -> SpawnerFactory.createRandomAvatarSpawner(
                    container.get( AvatarList.class ),
                    container.get( GameState.class )
                )
            )
            .setResolver(
                AvatarBeater.class,
                () -> AvatarBeaterFactory.create( container.get( AvatarList.class ) )
            )
            .setResolver(
                AvatarWalker.class,
                () -> AvatarWalkerFactory.create(
                    container.get( AvatarList.class ),
                    container.get( GameState.class )
                )
            )
            .setResolver(
                AvatarFader.class,
                () -> AvatarFaderFactory.create( container.get( AvatarList.class ) )
            )
            .setResolver(
                ScoreBoard.class,
                () -> AvatarScoreFactory.createAvatarScoreBoard( container.get( AvatarList.class ) )
            )

            .setResolver( AvatarController.class, () -> new AvatarController(
                container.get( GameState.class ),
                container.get( AvatarWalker.class ),
                container.get( AvatarBeater.class ),
                container.get( AvatarFader.class ),
                container.get( AudioPlayer.class ),
                container.get( ScoreBoard.class ),
                container.get( Spawner.class )
            ) )

            .setResolver( GameController.class, () -> new GameController(
                container.get( GameState.class ),
                container.get( Spawner.class ),
                container.get( AudioPlayer.class ),
                container.get( ScoreBoard.class )
            ) )

            .setResolver(
                GameLoop.class,
                () -> new GameLoopImpl(
                    container.get( AvatarController.class ),
                    container.get( View.class )
                )
            )

            .setResolver(
                GameLoopRunner.class,
                () -> new GameLoopRunnerImpl()
                {{
                    add( container.get( GameLoop.class ) );
                }}
            )

            .setResolver(
                GameLauncher.class,
                () -> new CoreLauncher( container.get( GameLoopRunner.class ) )
            )

            // the core has no view implementation,
            // so set dummy view to prevent UnresolvableServiceException.
            // please override service resolver in implementation
            .setResolver( View.class, DummyView::new )

            // the core has no audio player implementation,
            // so set dummy player to prevent UnresolvableServiceException
            // please override service resolver in implementation
            .setResolver( AudioPlayer.class, DummyPlayer::new )
        ;
    }
}
