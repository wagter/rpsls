package rpsls.core.score;

import rpsls.core.avatar.Avatar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Score implementation
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class ScoreImpl implements Score
{
    private Avatar avatar;
    private List<Long> scores;
    private Long lastScore;
    private int wrong = 0;

    /**
     * ScoreImpl constructor
     *
     * @param avatar the Avatar instance to keep the score for
     */
    public ScoreImpl( Avatar avatar )
    {
        this.avatar = avatar;
        this.scores = new ArrayList<>();
    }

    @Override
    public Avatar getAvatar()
    {
        return avatar;
    }

    @Override
    public List<Long> getScore()
    {
        return scores;
    }

    @Override
    public Long getHighScore()
    {
        return scores.size() > 0 ? scores.get( 0 ) : null;
    }

    @Override
    public Long getLastScore()
    {
        return lastScore;
    }

    @Override
    public void addScore( Long score )
    {
        scores.add( score );
        lastScore = score;
        Collections.sort( scores );
    }

    @Override
    public int getWrong()
    {
        return wrong;
    }

    @Override
    public void addWrong()
    {
        wrong++;
    }

    @Override
    public void reset()
    {
        scores = new ArrayList<>();
        lastScore = null;
        wrong = 0;
        System.out.println( avatar.getName() + " reset" );
    }
}