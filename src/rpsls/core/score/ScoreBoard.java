package rpsls.core.score;

import rpsls.core.avatar.Avatar;

import java.util.Iterator;
import java.util.List;

/**
 * Defining a score board to keep score for multiple Avatar instances
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface ScoreBoard
{
    /**
     * Add a score for an Avatar instance
     *
     * @param avatar the Avatar instance
     * @param score the score
     */
    void addScore( Avatar avatar, Long score );

    /**
     * Increment the total of wrong answers for an Avatar instance
     *
     * @param avatar the Avatar instance
     */
    void addWrong( Avatar avatar );

    /**
     * Get the total of wrong answers for an Avatar instance
     *
     * @param avatar the Avatar instance
     * @return the total of wrong answers
     */
    int getWrong( Avatar avatar );

    /**
     * Get the score for an Avatar instance
     *
     * @param avatar the Avatar instance
     * @return the score
     */
    List<Long> getScore( Avatar avatar );

    /**
     * Get the high score for an Avatar instance
     *
     * @param avatar the Avatar instance
     * @return the high score
     */
    Long getHighScore( Avatar avatar );

    /**
     * Get the high score in total
     *
     * @return the high score in total
     */
    Long getHighScore();

    /**
     * Get the last score of an Avatar instance
     *
     * @param avatar the Avatar instance
     * @return the last score
     */
    Long getLastScore( Avatar avatar );

    /**
     * Reset the score for an Avatar instance
     *
     * @param avatar the Avatar instance
     */
    void reset( Avatar avatar );

    /**
     * Reset score for all Avatar instances
     */
    void reset();

    /**
     * Get the iterator for iterating the Score objects
     *
     * @return the iterator for iterating the Score objects
     */
    Iterator<Score> getIterator();
}
