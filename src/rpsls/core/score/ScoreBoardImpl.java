package rpsls.core.score;

import rpsls.core.avatar.Avatar;

import java.util.*;

/**
 * ScoreBoard implementation
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class ScoreBoardImpl implements ScoreBoard
{
    private List<Score> scores;
    private Map<Avatar, Score> avatarScoreMap;

    /**
     * ScoreBoardImpl constructor
     *
     * @param scores the list of Score objects
     */
    public ScoreBoardImpl( List<Score> scores )
    {
        this.scores = scores;
        this.avatarScoreMap = new HashMap<>();
        for ( Score score : scores ) {
            avatarScoreMap.put( score.getAvatar(), score );
        }
    }

    @Override
    public void addScore( Avatar avatar, Long score )
    {
        avatarScoreMap.get( avatar ).addScore( score );
    }

    @Override
    public void addWrong( Avatar avatar )
    {
        avatarScoreMap.get( avatar ).addWrong();
    }

    @Override
    public int getWrong( Avatar avatar )
    {
        return avatarScoreMap.get(avatar ).getWrong();
    }

    @Override
    public List<Long> getScore( Avatar avatar )
    {
        return avatarScoreMap.get( avatar ).getScore();
    }

    @Override
    public Long getHighScore( Avatar avatar )
    {
        return avatarScoreMap.get( avatar ).getHighScore();
    }

    @Override
    public Long getHighScore()
    {
        List<Long> highScores = new ArrayList<>();

        Iterator it = avatarScoreMap.entrySet().iterator();
        while ( it.hasNext() ) {
            Map.Entry pair = (Map.Entry) it.next();
            Long highScore = getHighScore( (Avatar) pair.getKey() );
            if (highScore != null) {
                highScores.add( highScore );
            }
        }

        Collections.sort( highScores );

        return highScores.size() > 0 ? highScores.get( 0 ) : null;
    }

    @Override
    public Long getLastScore( Avatar avatar )
    {
        return avatarScoreMap.get( avatar ).getLastScore();
    }

    @Override
    public void reset( Avatar avatar )
    {
        avatarScoreMap.get( avatar ).reset();
    }

    @Override
    public void reset()
    {
        Iterator it = getIterator();
        System.out.println( it );
        while ( it.hasNext() ) {
            System.out.println( "iterate" );
            Score score = (Score)it.next();
            score.reset();
//            reset( (Avatar) pair.getKey() );
        }
    }

    public Iterator<Score> getIterator()
    {
        return scores.iterator();
    }
}
