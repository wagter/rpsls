package rpsls.core.score;

import rpsls.core.avatar.Avatar;

import java.util.List;

/**
 * Defining a score keeping object for an Avatar instance
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface Score
{
    /**
     * Get the Avatar that the object is keeping the score for
     *
     * @return the Avatar that the object is keeping the score for
     */
    Avatar getAvatar();

    /**
     * Get all scores
     *
     * @return a list of all scores
     */
    List<Long> getScore();

    /**
     * Get the high score
     *
     * @return the high score
     */
    Long getHighScore();

    /**
     * Get the last score
     *
     * @return the last score
     */
    Long getLastScore();

    /**
     * Add a score
     *
     * @param score a score
     */
    void addScore( Long score );

    /**
     * Get the total of wrong answers
     *
     * @return the total of wrong answers
     */
    int getWrong();

    /**
     * Increment total of wrong answers
     */
    void addWrong();

    /**
     * Reset the score
     */
    void reset();
}
