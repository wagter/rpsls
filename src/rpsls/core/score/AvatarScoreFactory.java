package rpsls.core.score;

import rpsls.core.avatar.Avatar;

import java.util.ArrayList;
import java.util.List;

/**
 * Factory class  to create Score and ScoreBoard instances
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class AvatarScoreFactory
{
    /**
     * Create a ScoreBoard for a list of Avatar instances
     *
     * @param avatars the list of Avatar instances
     * @return the ScoreBoard instance
     */
    public static ScoreBoard createAvatarScoreBoard( List<Avatar> avatars )
    {
        return new ScoreBoardImpl( new ArrayList<Score>()
        {{
            for ( Avatar avatar : avatars ) {
                add( createAvatarScore( avatar ) );
            }
        }} );
    }

    /**
     * Create a Score instance for an Avatar instance
     *
     * @param avatar the Avatar instance
     * @return the Score instance
     */
    public static Score createAvatarScore( Avatar avatar )
    {
        return new ScoreImpl( avatar );
    }
}
