package rpsls.core.walker;

public interface AvatarWalker
{
    void walkAvatars( double delta );
}
