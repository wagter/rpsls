package rpsls.core.walker;

import rpsls.core.avatar.Avatar;
import rpsls.core.state.GameState;

import java.util.List;

public class AvatarWalkerFactory
{
    public static AvatarWalker create( List<Avatar> avatars, GameState gameState )
    {
        return new AvatarWalkerImpl( avatars, gameState );
    }
}
