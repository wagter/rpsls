package rpsls.core.walker;

import rpsls.core.avatar.Avatar;
import rpsls.core.state.GameState;

import java.util.List;

public class AvatarWalkerImpl implements AvatarWalker
{
    private List<Avatar> avatars;
    private GameState gameState;

    public AvatarWalkerImpl( List<Avatar> avatars, GameState gameState )
    {
        this.avatars = avatars;
        this.gameState = gameState;
    }

    @Override
    public void walkAvatars( double delta )
    {
        for ( Avatar avatar : avatars ) {
            if ( avatar.isSpawned() ) {
                avatar.getPath().walk(
                    avatar,
                    gameState.getViewportDimension(),
                    delta
                );
            }
        }
    }
}
