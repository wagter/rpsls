package rpsls.core.view;

/**
 * Interface defining a visible element on the screen
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface View
{
    /**
     * Render the visible element(s) on the screen
     */
    void render();
}
