package rpsls.core.launcher;

/**
 * To launch the game
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface GameLauncher
{
    /**
     * Launch the game
     */
    void launch();
}
