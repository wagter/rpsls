package rpsls.core.launcher;

import rpsls.core.loop.GameLoopRunner;

public class CoreLauncher implements GameLauncher
{
    private GameLoopRunner loopRunner;

    public CoreLauncher( GameLoopRunner loopRunner )
    {
        this.loopRunner = loopRunner;
    }

    @Override
    public void launch()
    {
        loopRunner.runAll();
    }
}
