package rpsls.core.hint;

import java.util.Collection;

/**
 * Defining an object that is holding a collection of Hint objects
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface Hints
{
    /**
     * Get the hints for the current state of Avatars
     *
     * @return the hints for the current state of Avatars
     */
    Collection<Hint> getHints();
}
