package rpsls.core.hint;

import java.util.Collection;

/**
 * Hints implementation
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class HintsImpl implements Hints
{
    private Collection<Hint> hints;

    /**
     * HintsImpl constructor
     *
     * @param hints the collection of Hint objects
     */
    public HintsImpl( Collection<Hint> hints )
    {
        this.hints = hints;
    }

    @Override
    public Collection<Hint> getHints()
    {
        return hints;
    }
}
