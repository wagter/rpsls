package rpsls.core.hint;

import rpsls.core.avatar.Avatar;

/**
 * Defining a hint
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface Hint
{
    /**
     * Get the Avatar that beats
     *
     * @return the Avatar that beats
     */
    Avatar getBeater();

    /**
     * Get the Avatar that gets beaten
     *
     * @return the Avatar that gets beaten
     */
    Avatar getBeaten();

    /**
     * Get the beating act
     *
     * @return the beating act
     */
    String getBeatingAct();

    /**
     * Get the entire hint message
     *
     * @return the entire hint message
     */
    String getHint();
}
