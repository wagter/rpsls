package rpsls.core.hint;

import rpsls.core.avatar.Avatar;

import java.util.ArrayList;
import java.util.List;

/**
 * Builder class to build a Hints object
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class HintsBuilder
{
    private List<Hint> hints = new ArrayList<>();

    /**
     * Add a hint to the builder
     *
     * @param beater     the Avatar that beats
     * @param beaten     the Avatar that gets beaten
     * @param beatingAct the beating act
     */
    public void add( Avatar beater, Avatar beaten, String beatingAct )
    {
        hints.add( new HintImpl( beater, beaten, beatingAct ) );
    }

    /**
     * Build the Hints object
     *
     * @return the Hints object
     */
    public Hints build()
    {
        return new HintsImpl( hints );
    }
}
