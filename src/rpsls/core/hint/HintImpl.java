package rpsls.core.hint;

import rpsls.core.avatar.Avatar;

/**
 * Hint implementation
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class HintImpl implements Hint
{
    private Avatar beater;
    private Avatar beaten;
    private String beatingAct;
    private String hint;

    /**
     * HintImpl constructor
     *
     * @param beater     the Avatar that beats
     * @param beaten     the Avatar that gets beaten
     * @param beatingAct the beating act
     */
    public HintImpl( Avatar beater, Avatar beaten, String beatingAct )
    {
        this.beater = beater;
        this.beaten = beaten;
        this.beatingAct = beatingAct;
        this.hint = buildHint();
    }

    @Override
    public Avatar getBeater()
    {
        return beater;
    }

    @Override
    public Avatar getBeaten()
    {
        return beaten;
    }

    @Override
    public String getBeatingAct()
    {
        return beatingAct;
    }

    @Override
    public String getHint()
    {
        return hint;

    }

    /**
     * Build the hint message
     *
     * @return the hint message
     */
    private String buildHint()
    {

        return ucFirst( getBeater().getName() ) +
            " " +
            getBeatingAct() +
            " " +
            ucFirst( getBeaten().getName() );
    }

    /**
     * Make first letter of string uppercase
     *
     * @param string the string to modify
     * @return the modified string
     */
    private String ucFirst( String string )
    {
        char[] chars = string.toCharArray();
        chars[ 0 ] = Character.toUpperCase( chars[ 0 ] );

        return new String( chars );
    }
}
