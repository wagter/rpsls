package rpsls.core.state;

import rpsls.core.util.Dimension;

/**
 * To keep the state of  the game
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class GameState
{
    private boolean paused = false;
    private Dimension viewportDimension;
    private boolean fullscreen = false;
    private boolean muted = false;

    /**
     * GameState constructor
     */
    public GameState()
    {
        viewportDimension = new Dimension( 300,300 );
    }

    /**
     * Check if the game is paused
     *
     * @return true if the game is paused
     */
    public boolean isPaused()
    {
        return paused;
    }

    /**
     * Set if the game is paused
     *
     * @param paused true if the game is paused
     */
    public void setPaused( boolean paused )
    {
        this.paused = paused;
    }

    /**
     * Check if the game is muted
     *
     * @return true if the game is muted
     */
    public boolean isMuted()
    {
        return muted;
    }

    /**
     * Set if the game is muted
     *
     * @param muted true if the game is muted
     */
    public void setMuted( boolean muted )
    {
        this.muted = muted;
    }

    /**
     * Get the viewport dimension
     *
     * @return the viewport dimension
     */
    public Dimension getViewportDimension()
    {
        return viewportDimension;
    }

    /**
     * Set the viewport dimension
     *
     * @param viewportDimension the viewport dimension
     */
    public void setViewportDimension( Dimension viewportDimension )
    {
        this.viewportDimension = viewportDimension;
    }

    /**
     * Check if the game is full screen
     *
     * @return true if the game is full screen
     */
    public boolean isFullScreen()
    {
        return fullscreen;
    }

    /**
     * Set if the game is full screen
     *
     * @param fullscreen true if the game is full screen
     */
    public void setFullScreen( boolean fullscreen )
    {
        this.fullscreen = fullscreen;
    }
}
