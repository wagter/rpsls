package rpsls.core.spawner;

import rpsls.core.avatar.Spawnable;

/**
 * Defines a spawner
 */
public interface Spawner
{
    /**
     * Spawn a spawnable object
     *
     * @param nowMs the time from now in milliseconds
     * @return true if an object is spawned
     */
    boolean spawn( long nowMs );

    /**
     * Reset the spawning timer
     */
    void reset();

    void unSpawn( Spawnable spawnable );

    void unSpawnAll();
}
