package rpsls.core.spawner;

import rpsls.core.avatar.Spawnable;
import rpsls.core.state.GameState;
import rpsls.core.avatar.Avatar;
import rpsls.core.movable.path.PathFactory;
import rpsls.core.util.Position;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * A Spawner implementation to spawn random Avatar instances
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class RandomAvatarSpawner implements Spawner
{
    private List<Avatar> avatars;
    private GameState gameState;
    private Long spawnTime;

    /**
     * RandomAvatarSpawner constructor
     *
     * @param avatars   the list of Avatars to choose from
     * @param gameState the application's GameState instance
     */
    public RandomAvatarSpawner( List<Avatar> avatars, GameState gameState )
    {
        this.avatars = avatars;
        this.gameState = gameState;
    }

    @Override
    public boolean spawn( long nowMs )
    {
        if ( spawnTime == null ) {
            spawnTime = getNextSpawnTime( nowMs );
        }

        if ( nowMs < getSpawnTime() ) {
            return false;
        }

        List<Avatar> spawnableAvatars = getSpawnableAvatars();
        int avatarCount = spawnableAvatars.size();

        if ( avatarCount < 1 ) {
            return false;
        }

        int randomIndex = ThreadLocalRandom.current().nextInt( 0, avatarCount );
        int randomSpeed = ThreadLocalRandom.current().nextInt( 4, 8 );
        double randomX = ThreadLocalRandom.current().nextInt( 10, gameState.getViewportDimension().getWidth() - 64 - 10 );
        double randomY = ThreadLocalRandom.current().nextInt( 10, gameState.getViewportDimension().getHeight() - 64 - 10 );

        Avatar toSpawn = spawnableAvatars.get( randomIndex );
        toSpawn.setPosition( new Position( randomX, randomY ) );
        toSpawn.setPath( PathFactory.createAnglePath() );
        toSpawn.setSpeed( randomSpeed );
        toSpawn.setSpawned( true );
        toSpawn.setLastSpawned( nowMs );

        spawnTime = getNextSpawnTime( nowMs );

        return true;
    }

    @Override
    public void reset()
    {
        spawnTime = getNextSpawnTime( System.currentTimeMillis() );
    }

    @Override
    public void unSpawn( Spawnable spawnable )
    {
        for ( Avatar avatar : getSpawnedAvatars() ) {
            if ( avatar.hashCode() == spawnable.hashCode() ) {
                spawnable.setSpawned( false );
            }
        }
    }

    @Override
    public void unSpawnAll()
    {
        for ( Avatar avatar : getSpawnedAvatars() ) {
            avatar.setSpawned( false );
        }
    }

    /**
     * Get a list of spawned Avatar instances
     *
     * @return a list of spawned Avatar instances
     */
    private List<Avatar> getSpawnedAvatars()
    {
        return new ArrayList<Avatar>()
        {{
            for ( Avatar avatar : avatars ) {
                if ( avatar.isSpawned() ) {
                    add( avatar );
                }
            }
        }};
    }

    /**
     * Get a list of spawnable Avatar instances
     *
     * @return a list of spawnable Avatar instances
     */
    private List<Avatar> getSpawnableAvatars()
    {
        return new ArrayList<Avatar>()
        {{
            for ( Avatar avatar : avatars ) {
                if ( canSpawn( avatar ) ) {
                    add( avatar );
                }
            }
        }};
    }

    /**
     * Check if an Avatar can spawn
     *
     * @param avatar the Avatar to check
     * @return true if the Avatar can be spawned
     */
    private boolean canSpawn( Avatar avatar )
    {
        List<Avatar> spawnedAvatars = getSpawnedAvatars();

        if ( spawnedAvatars.size() < 1 ) {
            return true;
        }

        boolean canSpawn = true;
        for ( Avatar spawnedAvatar : spawnedAvatars ) {
            if ( !canCoexist( spawnedAvatar, avatar ) ) {
                canSpawn = false;
            }
        }

        return canSpawn;
    }

    /**
     * Check if two Avatar instances can be spawned at the same time
     *
     * @param avatarA one Avatar
     * @param avatarB the other Avatar
     * @return true if the Avatar instances can be spawned at the same time
     */
    private boolean canCoexist( Avatar avatarA, Avatar avatarB )
    {
        for ( Avatar avatar : avatars ) {
            if ( avatar.beats( avatarA ) && avatar.beats( avatarB ) ) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the time to spawn an Avatar instance
     *
     * @return the time to spawn
     */
    private Long getSpawnTime()
    {
        return spawnTime;
    }

    /**
     * Get the time to spawn the next Avatar instance
     *
     * @param now the time in milliseconds
     * @return the time to spawn the next Avatar instance
     */
    private Long getNextSpawnTime( long now )
    {
        return now + ThreadLocalRandom.current().nextInt( 1500, 6000 );
    }
}
