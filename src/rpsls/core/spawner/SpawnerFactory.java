package rpsls.core.spawner;

import rpsls.core.avatar.Avatar;
import rpsls.core.state.GameState;

import java.util.List;

public class SpawnerFactory
{
    public static Spawner createRandomAvatarSpawner( List<Avatar> avatars, GameState gameState )
    {
        return new RandomAvatarSpawner( avatars, gameState );
    }
}
