package rpsls.core.audio;

public class DummyPlayer implements AudioPlayer
{
    @Override
    public void play( String file )
    {

    }

    @Override
    public void play( String file, boolean rewind )
    {

    }

    @Override
    public void stop( String file )
    {

    }

    @Override
    public void rewind( String file )
    {

    }

    @Override
    public boolean isPlaying( String file )
    {
        return false;
    }
}
