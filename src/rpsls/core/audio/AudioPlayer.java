package rpsls.core.audio;

/**
 * Defining an audio player to play audio files
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface AudioPlayer
{
    /**
     * Play an audio file
     *
     * @param file the path to the audio file
     */
    void play( String file );

    /**
     * Play an audio file
     *
     * @param file the path to the audio file
     * @param rewind if the clip should be rewound first
     */
    void play( String file, boolean rewind );

    /**
     * Stop playing an audio file
     *
     * @param file the path to the audio file
     */
    void stop( String file );

    /**
     * Rewind an audio clip
     *
     * @param file the path to the audio file
     */
    void rewind( String file );

    /**
     * Check if an audio clip is currently playing
     *
     * @param file the path to the audio file
     * @return true if the audio clip is currently playing
     */
    boolean isPlaying( String file );
}
