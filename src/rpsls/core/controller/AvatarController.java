package rpsls.core.controller;

import rpsls.core.audio.AudioPlayer;
import rpsls.core.beater.AvatarBeater;
import rpsls.core.avatar.Avatar;
import rpsls.core.fader.AvatarFader;
import rpsls.core.score.ScoreBoard;
import rpsls.core.spawner.Spawner;
import rpsls.core.state.GameState;
import rpsls.core.walker.AvatarWalker;

/**
 * Controller for manipulating Avatar objects
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class AvatarController
{
    private GameState gameState;
    private AvatarWalker avatarWalker;
    private AvatarBeater avatarBeater;
    private AvatarFader avatarFader;
    private AudioPlayer audioPlayer;
    private ScoreBoard scoreBoard;
    private Spawner spawner;

    /**
     * AvatarController constructor
     *
     * @param gameState the GameState instance
     * @param avatarWalker the AvatarWalker instance
     * @param avatarBeater the AvatarBeater instance
     * @param avatarFader the AvatarFader instance
     * @param audioPlayer the AudioPlayer instance
     * @param scoreBoard the ScoreBoard instance
     * @param spawner the Spawner instance
     */
    public AvatarController(
        GameState gameState,
        AvatarWalker avatarWalker,
        AvatarBeater avatarBeater,
        AvatarFader avatarFader,
        AudioPlayer audioPlayer,
        ScoreBoard scoreBoard,
        Spawner spawner
    )
    {
        this.gameState = gameState;
        this.avatarWalker = avatarWalker;
        this.avatarBeater = avatarBeater;
        this.avatarFader = avatarFader;
        this.audioPlayer = audioPlayer;
        this.scoreBoard = scoreBoard;
        this.spawner = spawner;
    }

    /**
     * Action to beat an Avatar by another avatar
     *
     * @param beats the avatar that does the beating
     */
    public void beatAction( Avatar beats )
    {
        if ( gameState.isPaused() || beats.isSpawned() ) {
            return;
        }

        Long score = avatarBeater.beat( beats );

        if ( score != null ) {
            audioPlayer.play( "beat", true );
            scoreBoard.addScore( beats, score );
            spawner.reset();
            return;
        }

        audioPlayer.play( "wrong", true );
        scoreBoard.addWrong( beats );

    }

    /**
     * Action to walk the Avatar instances over the viewport
     *
     * @param delta the delta value of the game loop
     */
    public void walkAction( double delta )
    {
        if ( gameState.isPaused() ) {
            return;
        }

        avatarWalker.walkAvatars( delta );
    }

    /**
     * Action to fade Avatar instances in and out
     *
     * @param delta the delta value of the game loop
     */
    public void fadeAction( double delta )
    {
        avatarFader.fadeAvatars( delta );
    }

    /**
     * Action to spawn Avatar instances
     *
     * @param nowMs the time from "now" in milliseconds
     */
    public void spawnAction( long nowMs )
    {
        if ( !gameState.isPaused() && spawner.spawn( nowMs ) ) {
            audioPlayer.play( "spawn", true );
        }
    }
}
