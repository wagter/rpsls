package rpsls.core.controller;

import rpsls.core.audio.AudioPlayer;
import rpsls.core.score.ScoreBoard;
import rpsls.core.spawner.Spawner;
import rpsls.core.state.GameState;

/**
 * Controller for manipulating the rpsls.core state
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class GameController
{
    protected GameState gameState;
    protected Spawner spawner;
    protected AudioPlayer audioPlayer;
    protected ScoreBoard scoreBoard;

    /**
     * GameController constructor
     *
     * @param gameState the GameState instance
     * @param spawner the Spawner instance
     * @param audioPlayer the AudioPlayer instance
     * @param scoreBoard the ScoreBoard instance
     */
    public GameController(
        GameState gameState,
        Spawner spawner,
        AudioPlayer audioPlayer,
        ScoreBoard scoreBoard
    )
    {
        this.gameState = gameState;
        this.spawner = spawner;
        this.audioPlayer = audioPlayer;
        this.scoreBoard = scoreBoard;
    }

    /**
     * Toggle the game's play/pause state
     */
    public void togglePlayAction()
    {
        gameState.setPaused( !gameState.isPaused() );

        if ( !gameState.isPaused() ) {
            spawner.reset();
        }

        audioPlayer.play( "pause", true );
    }

    /**
     * Toggle the game's fullscreen state
     */
    public void toggleFullScreenAction()
    {
    }

    /**
     * Stop the game
     */
    public void stopAction()
    {
        spawner.unSpawnAll();
        gameState.setPaused( true );
        scoreBoard.reset();
    }

    /**
     * Toggle the game's sound state
     */
    public void toggleSoundAction()
    {
        gameState.setMuted( !gameState.isMuted() );
    }
}
