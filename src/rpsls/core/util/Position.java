package rpsls.core.util;

/**
 * Representing a position
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class Position
{
    private double x, y;

    /**
     * Position constructor
     *
     * @param x the position's x-axis value
     * @param y the position's y-axis value
     */
    public Position( double x, double y)
    {
        this.x = x;
        this.y = y;
    }

    /**
     * Get the position's x-axis value
     *
     * @return the position's x-axis value
     */
    public double getX()
    {
        return x;
    }

    /**
     * Set the position's x-axis value
     *
     * @param x the position's x-axis value
     */
    public void setX( double x )
    {
        this.x = x;
    }

    /**
     * Get the position's y-axis value
     *
     * @return the position's y-axis value
     */
    public double getY()
    {
        return y;
    }

    /**
     * Set the position's x-axis value
     *
     * @param y the position's y-axis value
     */
    public void setY( double y )
    {
        this.y = y;
    }
}
