package rpsls.core.util;

/**
 * Representing a dimension.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class Dimension
{
    private int width, height;

    /**
     * Dimension constructor
     *
     * @param width the dimension's width
     * @param height the dimension's height
     */
    public Dimension( int width, int height)
    {
        this.width = width;
        this.height = height;
    }

    /**
     * Get the dimension's width
     *
     * @return the dimension's width
     */
    public int getWidth()
    {
        return width;
    }

    /**
     * Set the dimension's width
     *
     * @param width the dimension's width
     */
    public void setWidth( int width )
    {
        this.width = width;
    }

    /**
     * Get the dimension's height
     *
     * @return the dimension's height
     */
    public int getHeight()
    {
        return height;
    }

    /**
     * Set the dimension's height
     *
     * @param height the dimension's height
     */
    public void setHeight( int height )
    {
        this.height = height;
    }
}
