package rpsls.core.movable;

import rpsls.core.movable.path.Path;
import rpsls.core.util.Position;

/**
 * Interface that defines that an object is movable.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface Movable
{
    /**
     * Get the position of the movable object
     *
     * @return the position of the movable object
     */
    Position getPosition();

    /**
     * Set the position of the movable object
     *
     * @param position the position of the movable object
     */
    void setPosition( Position position );

    /**
     * Get the moving speed of the movable object
     *
     * @return the moving speed of the movable object
     */
    int getSpeed();

    /**
     * Set the moving speed of the movable object
     *
     * @param speed the moving speed of the movable object
     */
    void setSpeed( int speed );

    /**
     * Get the path of  the movable object
     *
     * @return the path of  the movable object
     */
    Path getPath();

    /**
     * Set the path of the movable object
     *
     * @param path the path of  the movable object
     */
    void setPath( Path path );
}
