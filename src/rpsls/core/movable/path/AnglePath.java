package rpsls.core.movable.path;

import rpsls.core.movable.Movable;
import rpsls.core.util.Dimension;
import rpsls.core.util.Position;

/**
 * Representing a linear path in a given angle
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class AnglePath implements Path
{
    private int angle;
    private boolean xReverse, yReverse;

    /**
     * AnglePath constructor
     *
     * @param angle the angle of the path
     */
    public AnglePath( int angle )
    {
        this(angle, false, false);
    }

    /**
     * AnglePath constructor
     *
     * @param angle the angle of the path
     * @param xReverse true if the x-axis should be reversed
     * @param yReverse true if the y-axis should be reversed
     */
    public AnglePath( int angle, boolean xReverse, boolean yReverse )
    {
        this.angle = angle;
        this.xReverse = xReverse;
        this.yReverse = yReverse;
    }

    @Override
    public void walk( Movable movable, Dimension viewportDimension, double delta )
    {
        Position position = movable.getPosition();

        if ( position.getX() > viewportDimension.getWidth() - 64 - 2 ) {
            xReverse = true;
        } else if ( xReverse && position.getX() < 2 ) {
            xReverse = false;
        }

        if ( position.getY() > viewportDimension.getHeight() - 64 - 2 ) {
            yReverse = true;
        } else if ( yReverse && position.getY() < 2 ) {
            yReverse = false;
        }

        Position movement = getXYMovement( angle, movable.getSpeed(), delta );

        movable.setPosition( new Position(
            !xReverse
                ? position.getX() + movement.getX()
                : position.getX() - movement.getX(),
            !yReverse
                ? position.getY() + movement.getY()
                : position.getY() - movement.getY()
        ) );
    }

    /**
     * Get the X and Y movement of the path
     *
     * @param angle the angle of the path
     * @param speed the speed of the movable
     * @param delta the delta of the rpsls.core loop
     * @return the movement of the path
     */
    private Position getXYMovement( double angle, double speed, double delta )
    {
        double radians = Math.toRadians( angle );

        return new Position(
            Math.cos( radians ) * speed * delta,
            Math.sin( radians ) * speed * delta
        );
    }
}
