package rpsls.core.movable.path;

import rpsls.core.movable.Movable;
import rpsls.core.util.Dimension;

/**
 * Defining a path of a movable object
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface Path
{
    /**
     * Walk the path
     *
     * @param movable the movable to walk
     * @param viewportDimension the dimension of the viewport
     * @param delta the delta of the game loop
     */
    void walk( Movable movable, Dimension viewportDimension, double delta );
}
