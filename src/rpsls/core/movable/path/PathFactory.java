package rpsls.core.movable.path;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Factory to create Path instances
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class PathFactory
{
    /**
     * Create an AnglePath instance
     *
     * @return the AnglePath instance
     */
    public static Path createAnglePath()
    {
        int randomAngle = ThreadLocalRandom.current().nextInt(3, 87);
        boolean xReverse = ThreadLocalRandom.current().nextInt(0, 2) > 0;
        boolean yReverse = ThreadLocalRandom.current().nextInt(0, 2) > 0;

        return new AnglePath( randomAngle, xReverse, yReverse );
    }
}
