package rpsls.core.loop;

import rpsls.core.controller.AvatarController;
import rpsls.core.view.View;

/**
 * GameLoop implementation
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class GameLoopImpl implements GameLoop
{
    private boolean running = false;
    private final int TARGET_FPS = 60;
    private final long OPTIMAL_TIME = 1_000_000_000 / TARGET_FPS;
    private AvatarController avatarController;
    private View mainView;

    /**
     * GameLoopImpl constructor
     *
     * @param avatarController the AvatarController instance
     * @param mainView the main View instance
     */
    public GameLoopImpl( AvatarController avatarController, View mainView )
    {
        this.avatarController = avatarController;
        this.mainView = mainView;
    }

    @Override
    public void run()
    {
        long lastLoopTime = System.nanoTime();

        while ( isRunning() ) {
            long nowNs = System.nanoTime();
            long nowMs = System.currentTimeMillis();
            long updateLength = nowNs - lastLoopTime;
            lastLoopTime = nowNs;
            double delta = updateLength / ( (double) OPTIMAL_TIME );

            avatarController.walkAction( delta );
            avatarController.fadeAction( delta );
            avatarController.spawnAction( nowMs );

            mainView.render();

            long sleepTime = ( lastLoopTime - System.nanoTime() + OPTIMAL_TIME ) / 1_000_000;
            try {
                Thread.sleep( sleepTime < 0 ? 0 : sleepTime );
            } catch ( InterruptedException e ) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void setRunning( boolean running )
    {
        this.running = running;
    }

    @Override
    public boolean isRunning()
    {
        return running;
    }
}
