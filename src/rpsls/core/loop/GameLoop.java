package rpsls.core.loop;

/**
 * Defining a loop to run the game smoothly on different systems
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface GameLoop extends Runnable
{
    /**
     * Set if the loop should be running
     *
     * @param running true if the loop should be running
     */
    void setRunning( boolean running );

    /**
     * Check if the loop is running
     *
     * @return true if the loop is running
     */
    boolean isRunning();
}
