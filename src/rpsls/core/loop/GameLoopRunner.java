package rpsls.core.loop;

/**
 * Defining an object to run an stop GameLoop instances
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface GameLoopRunner
{
    /**
     * Add a GameLoop instance to the runner
     *
     * @param gameLoop a GameLoop instance
     */
    void add( GameLoop gameLoop );

    /**
     * Run a GameLoop instance
     *
     * @param gameLoop a GameLoop instance
     */
    void run( GameLoop gameLoop );

    /**
     * Run all added GameLoop instances
     */
    void runAll();

    /**
     * Stop a GameLoop instance
     *
     * @param gameLoop a GameLoop instance
     */
    void stop( GameLoop gameLoop );

    /**
     * Stop all GameLoop instances
     */
    void stopAll();
}
