package rpsls.core.loop;

import java.util.ArrayList;
import java.util.Collection;

/**
 * GameLoopRunner implementation
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class GameLoopRunnerImpl implements GameLoopRunner
{
    private Collection<GameLoop> gameLoops;

    /**
     * GameLoopRunner constructor
     */
    public GameLoopRunnerImpl()
    {
        gameLoops = new ArrayList<>();
    }

    @Override
    public void add( GameLoop gameLoop )
    {
        gameLoops.add( gameLoop );
    }

    @Override
    public void run( GameLoop gameLoop )
    {
        if ( !gameLoop.isRunning() ) {
            gameLoop.setRunning( true );
            Thread thread = new Thread( gameLoop );
            thread.start();
        }
    }

    @Override
    public void runAll()
    {
        for ( GameLoop gameLoop : gameLoops ) {
            run( gameLoop );
        }
    }

    @Override
    public void stop( GameLoop gameLoop )
    {
        gameLoop.setRunning( false );
    }

    @Override
    public void stopAll()
    {
        for ( GameLoop gameLoop : gameLoops ) {
            stop( gameLoop );
        }
    }
}
