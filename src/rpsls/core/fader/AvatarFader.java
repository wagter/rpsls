package rpsls.core.fader;

public interface AvatarFader
{
    void fadeAvatars( double delta );
}
