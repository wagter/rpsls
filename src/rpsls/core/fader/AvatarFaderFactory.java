package rpsls.core.fader;

import rpsls.core.avatar.Avatar;

import java.util.List;

public class AvatarFaderFactory
{
    public static AvatarFader create( List<Avatar> avatars )
    {
        return new AvatarFaderImpl( avatars );
    }
}
