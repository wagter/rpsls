package rpsls.core.fader;

import rpsls.core.avatar.Avatar;

import java.util.List;

public class AvatarFaderImpl implements AvatarFader
{
    private List<Avatar> avatars;

    public AvatarFaderImpl( List<Avatar> avatars )
    {
        this.avatars = avatars;
    }

    @Override
    public void fadeAvatars( double delta )
    {
        for ( Avatar avatar : avatars ) {

            if ( avatar.isSpawned() && avatar.getAlpha() < 1 ) {
                avatar.setAlpha( avatar.getAlpha() + (float) ( 0.03 * delta ) );
            } else if ( !avatar.isSpawned() && avatar.getAlpha() > 0 ) {
                avatar.setAlpha( avatar.getAlpha() - (float) ( 0.05 * delta ) );
            }

            if ( avatar.getAlpha() > 1 ) {
                avatar.setAlpha( 1 );
            } else if ( avatar.getAlpha() < 0 ) {
                avatar.setAlpha( 0 );
            }

        }
    }
}
