package rpsls.core.avatar;

/**
 * Avatar representing Scissors
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class ScissorsAvatar extends AbstractAvatar
{
    /**
     * ScissorsAvatar constructor
     *
     * @param color the hexadecimal color value
     */
    public ScissorsAvatar( String color )
    {
        super( "scissors", color );
    }

    @Override
    public boolean beats( Avatar avatar )
    {
        return avatar instanceof PaperAvatar || avatar instanceof LizardAvatar;
    }
}
