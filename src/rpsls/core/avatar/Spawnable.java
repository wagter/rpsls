package rpsls.core.avatar;

/**
 * Defines an object that can be spawned
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface Spawnable
{
    /**
     * Check if the spawnable object is spawned
     *
     * @return true if the spawnable object is spawned
     */
    boolean isSpawned();

    /**
     * Set the spawnable object's spawned state
     *
     * @param spawned true if the spawnable object should be spawned
     */
    void setSpawned( boolean spawned );

    /**
     * Set the time when the spawnable object was last spawned
     *
     * @param lastSpawned the time when the spawnable object was last spawned
     */
    void setLastSpawned( Long lastSpawned );

    /**
     * Get the time when the spawnable object was last spawned
     *
     * @return the time when the spawnable object was last spawned
     */
    Long getLastSpawned();
}
