package rpsls.core.avatar;

/**
 * Avatar representing Rock
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class RockAvatar extends AbstractAvatar
{
    /**
     * RockAvatar constructor
     *
     * @param color the hexadecimal color value
     */
    public RockAvatar( String color )
    {
        super( "rock", color );
    }

    @Override
    public boolean beats( Avatar avatar )
    {
        return avatar instanceof LizardAvatar || avatar instanceof ScissorsAvatar;
    }
}
