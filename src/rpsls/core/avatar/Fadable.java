package rpsls.core.avatar;

/**
 * Defines an object that can be faded in and out
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface Fadable
{
    /**
     * Set the alpha value of the fadable object
     *
     * @param alpha the alpha value of the fadable object
     */
    void setAlpha( float alpha );

    /**
     * Get the alpha value of the fadable object
     *
     * @return the alpha value of the fadable object
     */
    float getAlpha();
}
