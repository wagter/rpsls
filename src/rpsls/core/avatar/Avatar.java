package rpsls.core.avatar;

import rpsls.core.movable.Movable;

/**
 * Interface defining an Avatar object
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface Avatar extends Movable, Spawnable, Fadable
{
    /**
     * Get the name of the Avatar
     *
     * @return the name of the Avatar
     */
    String getName();

    /**
     * Get the color of the Avatar
     *
     * @return the color of the Avatar
     */
    String getColor();

    /**
     * Check if the Avatar beats another Avatar
     *
     * @param avatar the Avatar to beat
     * @return true if the Avatar beats the other avatar
     */
    boolean beats( Avatar avatar );
}
