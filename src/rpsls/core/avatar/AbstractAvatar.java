package rpsls.core.avatar;

import rpsls.core.movable.path.Path;
import rpsls.core.util.Position;

/**
 * Abstract Avatar class implementing common methods
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public abstract class AbstractAvatar implements Avatar
{
    private Position position;
    private boolean visible = false;
    private Long lastSpawned;
    private String name;
    private String color;
    private float alpha;
    private int speed;
    private Path path;

    /**
     * AbstractAvatar constructor
     *
     * @param name the name of the avatar
     * @param color the color of the avatar
     */
    public AbstractAvatar( String name, String color )
    {
        this( name, color, new Position( 0, 0 ) );
    }

    /**
     * AbstractAvatar constructor
     *
     * @param name the name of the avatar
     * @param color the color of the avatar
     * @param position the position of the avatar
     */
    public AbstractAvatar( String name, String color, Position position )
    {
        this.name = name;
        this.color = color;
        this.position = position;
    }

    @Override
    public String getName()
    {
        return name;
    }

    @Override
    public String getColor()
    {
        return color;
    }

    @Override
    public Position getPosition()
    {
        return position;
    }

    @Override
    public void setPosition( Position position )
    {
        this.position = position;
    }

    @Override
    public int getSpeed()
    {
        return speed;
    }

    @Override
    public void setSpeed( int speed )
    {
        this.speed = speed;
    }

    @Override
    public Path getPath()
    {
        return path;
    }

    @Override
    public void setPath( Path path )
    {
        this.path = path;
    }

    @Override
    public boolean isSpawned()
    {
        return visible;
    }

    @Override
    public void setSpawned( boolean spawned )
    {
        this.visible = spawned;
    }

    @Override
    public float getAlpha()
    {
        return alpha;
    }

    @Override
    public void setAlpha( float alpha )
    {
        this.alpha = alpha;
    }

    @Override
    public Long getLastSpawned()
    {
        return lastSpawned;
    }

    @Override
    public void setLastSpawned( Long lastSpawned )
    {
        this.lastSpawned = lastSpawned;
    }
}