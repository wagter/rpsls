package rpsls.core.avatar;

/**
 * Avatar representing Paper
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class PaperAvatar extends AbstractAvatar
{
    /**
     * PaperAvatar constructor
     *
     * @param color the hexadecimal color value
     */
    public PaperAvatar( String color )
    {
        super( "paper", color );
    }

    @Override
    public boolean beats( Avatar avatar )
    {
        return avatar instanceof RockAvatar || avatar instanceof SpockAvatar;
    }
}
