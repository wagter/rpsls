package rpsls.core.avatar;

/**
 * Avatar representing Lizard
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class LizardAvatar extends AbstractAvatar
{
    /**
     * LizardAvatar constructor
     *
     * @param color the hexadecimal color value
     */
    public LizardAvatar( String color )
    {
        super( "lizard", color );
    }

    @Override
    public boolean beats( Avatar avatar )
    {
        return avatar instanceof PaperAvatar || avatar instanceof SpockAvatar;
    }
}
