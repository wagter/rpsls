package rpsls.core.avatar;

/**
 * Avatar representing Spock
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class SpockAvatar extends AbstractAvatar
{
    /**
     * SpockAvatar constructor
     *
     * @param color the hexadecimal color value
     */
    public SpockAvatar( String color )
    {
        super( "spock", color );
    }

    @Override
    public boolean beats( Avatar avatar )
    {
        return avatar instanceof RockAvatar || avatar instanceof ScissorsAvatar;
    }
}
